<?php

namespace Drupal\cforge_group\Plugin\views\access;
use Drupal\group\Plugin\views\access\GroupPermission;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\Routing\Route;

/**
 * Access plugin that provides group permission-based access control.
 *
 * @ingroup views_access_plugins
 *
 * @ViewsAccess(
 *   id = "cforge_group_permission",
 *   title = @Translation("Cforge Group permission"),
 *   help = @Translation("Access will be granted to user 1 or users with the specified group permission string.")
 * )
 */
class CforgeGroupPermission extends GroupPermission {

  /**
   * {@inheritdoc}
   */
  public function access(AccountInterface $account) {
    if (\Drupal::currentUser()->id() == 1) return TRUE;
    return parent::access($account);
  }

  public function alterRouteDefinition(Route $route) {
    $route->setRequirement('_cforge_group_permission', $this->options['group_permission']);
    $route->setOption('parameters', ['group' => ['type' => 'entity:group']]);
  }


}
