<?php

namespace Drupal\cforge_group\Plugin\Group\RelationHandler;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\Url;
use Drupal\group\Entity\GroupInterface;
use Drupal\group\Plugin\Group\RelationHandler\OperationProviderInterface;
use Drupal\group\Plugin\Group\RelationHandler\OperationProviderTrait;
use Drupal\group\Entity\GroupRelationshipType;
use Drupal\node\Entity\NodeType;

/**
 * Provides operations for the group_membership relation plugin.
 */
class GroupNodeOperationProvider implements OperationProviderInterface {

  use OperationProviderTrait;

  /**
   * Constructs a new GroupMembershipOperationProvider.
   *
   * @param \Drupal\group\Plugin\Group\RelationHandler\OperationProviderInterface $parent
   *   The default operation provider.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   */
  public function __construct(OperationProviderInterface $parent, AccountProxyInterface $current_user, TranslationInterface $string_translation) {
    $this->parent = $parent;
    $this->currentUser = $current_user;
    $this->stringTranslation = $string_translation;
  }

  /**
   * {@inheritdoc}
   */
  public function getGroupOperations(GroupInterface $group) {
    $operations = [];
    foreach (GroupRelationshipType::loadByEntityTypeId('node') as $node_type) {
      $def = $node_type->getPlugin()->getPluginDefinition();
      $bundle = NodeType::load($def->getEntityBundle());
      if ($node_type->getGroupTypeId() == $group->bundle()) {
        $operations['node:' . $group->bundle().'-list'] = [
          'title' => t('Show @types', ['@type' => $bundle->label()]),
          'url' => Url::fromRoute('view.group_nodes.page_1', ['group' => $group->id()], ['query' => ['type' => $bundle->id()]]),
          'weight' => 0
        ];
      }
    }
    return $operations;
  }

}

/**
 * Implements hook_block_view_BASE_BLOCK_ID_alter().
 *
 * Change the title of the user block.
 */
function cforge_group_block_view_group_operations_alter(&$build, $block) {
  //the theme doesn't even run
  $build['#pre_render'][] = function ($array) {unset($array['content']['#type']);$array['content']['#theme'] = 'links';};
  return $build;
}
