<?php

namespace Drupal\cforge_group\Plugin\Group\RelationHandler;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\Url;
use Drupal\group\Entity\GroupInterface;
use Drupal\group\Plugin\Group\RelationHandler\OperationProviderInterface;
use Drupal\group\Plugin\Group\RelationHandler\OperationProviderTrait;
use Drupal\group\Entity\GroupRelationshipType;
use Drupal\smallads\Entity\SmalladType;

/**
 * Provides operations for the group_membership relation plugin.
 */
class GroupSmalladOperationProvider implements OperationProviderInterface {

  use OperationProviderTrait;

  /**
   * Constructs a new GroupMembershipOperationProvider.
   *
   * @param \Drupal\group\Plugin\Group\RelationHandler\OperationProviderInterface $parent
   *   The default operation provider.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   */
  public function __construct(OperationProviderInterface $parent, AccountProxyInterface $current_user, TranslationInterface $string_translation) {
    $this->parent = $parent;
    $this->currentUser = $current_user;
    $this->stringTranslation = $string_translation;
  }

  /**
   * {@inheritdoc}
   */
  public function getGroupOperations(GroupInterface $group) {
    $operations = [];
    foreach (GroupRelationshipType::loadByEntityTypeId('smallad') as $smallad_type) {
      $def = $smallad_type->getPlugin()->getPluginDefinition();
      $bundle = SmalladType::load($def->getEntityBundle());
      if ($smallad_type->getGroupTypeId() == $group->bundle()) {
        $operations['smallad:' . $group->bundle().'-list'] = [
          'title' => t('Show @type', ['@type' => $bundle->labelPlural()]),
          'url' => Url::fromRoute('view.group_smallads.page_1', ['group' => $group->id()], ['query' => ['type' => $bundle->id()]]),
          'weight' => 0
        ];
      }
    }
    return $operations;
  }

}
