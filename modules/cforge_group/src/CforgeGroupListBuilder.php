<?php

namespace Drupal\cforge_group;

use Drupal\Core\Entity\EntityInterface;

class CforgeGroupListBuilder extends \Drupal\group\Entity\Controller\GroupListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row = parent::buildRow($entity);
    // Add data to your custom column.
    $row['uid'] = $entity->getOwner()->toLink()->toString();

    return $row;
  }
}

