<?php

namespace Drupal\cforge_group\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\Routing\Route;
use Drupal\group\Access\GroupPermissionAccessCheck;

/**
 * Determines access to routes based on group permissions.
 */
class CforgeGroupPermissionAccessCheck extends GroupPermissionAccessCheck {

  /**
   * Checks access.
   *
   * @param \Symfony\Component\Routing\Route $route
   *   The route to check against.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The parametrized route.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account to check access for.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(Route $route, RouteMatchInterface $route_match, AccountInterface $account) {
    if ($account->hasPermission('administer group')) {
      return AccessResult::allowed();
    }
    return parent::access($route, $route_match, $account);
  }

}
