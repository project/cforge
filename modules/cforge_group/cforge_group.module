<?php

use Drupal\smallads\Entity\SmalladInterface;
use Drupal\group\Entity\Group;
use Drupal\views\Views;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Url;
use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Implements hook_entity_type_info_alter().
 */
function cforge_group_entity_type_alter(array &$entity_types) {
  $entity_types['group']->setListBuilderClass('\Drupal\cforge_group\CforgeGroupListBuilder');
}

/**
 * Implements hook_ENTITY_TYPE_view_alter().
 * Put the latest modified content on the group front page.
 */
function cforge_group_group_view_alter(&$build, $group, $display) {
  if ($info = $display->getComponent('latest')) {
    $build['latest']['views'] = cforge_group_latest($group);
    $build['latest']['#weight'] = $info['weight'];
    $build['latest']['#attached']['library'][] = 'cforge_group/front';
  }
  // Has no effect
  $build['#title'] = t('@type: @title', ['@type' => \Drupal\group\Entity\GroupType::load($group->bundle())->label(), '@label' => $group->label()]);
}

/**
 * Use the cforge_group_front view to display the latest group content, by type.
 * @param Group $group
 */
function cforge_group_latest(Group $group) :array {
  $node_types = [
    '#prefix' => '<div id="group-front">',
    '#suffix' => '</div>',
    '#markup' => '<h2>'.t('Latest content').'</h2>'];
  // Get all content types that might be in the group
  foreach ($group->getGroupType()->getInstalledPlugins() as $group_content_type) {
    $def = $group_content_type->getPluginDefinition();
    if ($def->getEntityTypeId() == 'node') {
      $view = Views::getView('cforge_group_front');
      $view->setDisplay('front');
      $view->setArguments([$group->id(), $def->getEntityBundle()]);
      $view->execute();
      if ($view->result) {
        $node_types[$def->getEntityBundle()] = $view->render();
      }
    }
  }
  if (empty($node_types)) {
    $node_types['#markup'] = t('This group has no content.');
  }
  return $node_types;
}

/**
 * Utility
 * @return array
 */
function cforge_group_types() : array {
  $bundle_info = \Drupal::service('entity_type.bundle.info')->getAllBundleInfo();
  return array_keys($bundle_info['group']);
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * @note https://www.drupal.org/project/group/issues/2980034#comment-15527952
 */
function cforge_group_form_group_relationship_form_alter(&$form, Drupal\Core\Form\FormState $form_state) {
  // Not sure why this would ever be null, but it was.
  if ($group = $form_state->getFormObject()->getEntity()->getGroup()) {
    // Anyway this is currently overwritten in GroupRelationshipForm::Save.
    $form_state->setRedirect('entity.group_relationship.collection', [$group->id()]);
  }
}

/**
 * Implements hook_group_access().
 *  remove this when all platforms updated cf_hosted.
 */
function cforge_group_group_access($entity, $operation, $account) {
  return AccessResult::allowedIf($account->hasPermission('administer group'));
}

/**
 * Implements hook_group_operations_alter().
 *
 * Because the group operations are used in the group_operations block provided by the group module.
 * Each $link has Translatablemarkup title, Url, weight
 * @todo this should probably go in its own plugin, group.relation_handler.operation_provider.group_user ?
 */
function cforge_group_group_operations_alter(&$links, $group) {
  // Requires that this view access control is changed manually OR that group members have the permission in the default view.
  $links['members'] = [
    'title' => t('Show @types', ['@types' => 'Members']),
    'url' => Url::fromRoute('view.group_members.page_1', ['group' => $group->id()]),
    'weight' => -1
  ];

  return $links;
}

/**
 * Implements hook_user_presave().
 * Automatically manage membership of the committee group
 */
function cforge_group_user_presave($user) {
  if ($committee = cforge_group_committee_group()) {
    if ($user->hasRole(RID_COMMITTEE) and !$user->original->hasRole(RID_COMMITTEE)) {
      if ($memship = \Drupal::service('group.membership_loader')->load($committee, $user)) {
        $memship->delete();
      }
    }
    elseif (!$user->hasRole(RID_COMMITTEE) and $user->original->hasRole(RID_COMMITTEE)) {
      $committee->addMember($user);
    }
  }
}

/**
 * Get the default group, called 'committee'
 * @return Group
 */
function cforge_group_committee_group() : Group|NULL {
  if ($gid = \Drupal::config('cforge_group.settings')->get('committee_gid')) {
    return Group::load($gid);
  }
  return NULL;
}

/**
 * Implements hook_form_ENTITY_TYPE_alter()
 */
function cforge_group_form_node_form_alter(&$form, $form_state) {
  cforge_group_add_group_chooser($form, $form_state);
}
function cforge_group_form_smallad_form_alter(&$form, $form_state) {
  cforge_group_add_group_chooser($form, $form_state);
  if (isset($form['group_add'])) {
    $form['group_add']['#states'] = [
      'visible' => [':input[name="scope"]' => ['value' => SmalladInterface::SCOPE_GROUP]],
    ];
    $form['group_add']['#element_validate'][] = 'cforge_group_smallad_group_selected';
  }
}
/**
 * Form submit handler.
 *
 * If smallad scope is set to group, ensure that at least one group is selected.
 */
function cforge_group_smallad_group_selected(&$element, $form_state) {
  if ($form_state->getValue('scope')[0]['value'] == SmalladInterface::SCOPE_GROUP) {
    if (empty($element['#value'])) {
      $form_state->setError($element, t('You must choose a group.'));
    }
  }
}

/**
 * Add a widget to the entity form.
 */
function cforge_group_add_group_chooser(&$form, $form_state) {
  $form_object = $form_state->getFormObject();
  $ops = ['edit', 'default'];
  if (!in_array($form_object->getOperation(), $ops))return;
  $entity = $form_object->getEntity();
  $content_plugin = 'group_'.$entity->getEntityTypeId().':'.$entity->bundle();
  $etm = \Drupal::entityTypeManager();
  $plugins = $etm->getStorage('group_relationship_type')->loadByProperties(['content_plugin' => $content_plugin]);

  if ($plugin = reset($plugins)) {
    if ($common_groups = cforge_group_common_group_ids($entity)) {
      foreach ($form_object->getFormDisplay($form_state)->getComponents() as $name => $comp) {
        $weights[$name] = $comp['weight'];
      }
      $form['group_add'] = [
        '#title' => t('Belongs to group(s)'),
        '#description' => t('Content will not be visible outside those groups'),
        '#type' => 'checkboxes',
        '#options' => array_map(fn($g) => $g->label(), $common_groups),
        '#weight' => max($weights)-1,
        '#default_value' => []
      ];

      if (!$entity->isNew()) {
        $relationships = $etm->getStorage('group_relationship')->loadByProperties(['plugin_id' => $content_plugin, 'entity_id' => $entity->id()]);
        $form['group_add']['#default_value'] = array_map(fn($rel) => $rel->getGroup()->id(), $relationships);
      }
      $form['actions']['submit']['#submit'][] = 'cforge_group_entity_form_submit';
    }
  }
}

/**
 * Get the groups the current user has in common with the type of the given entity.
 * @param type $entity
 * @return Group[]
 */
function cforge_group_common_group_ids(ContentEntityInterface $entity) : array {
  $plugin_type = 'group_'.$entity->getEntityTypeId();
  $etm = \Drupal::entityTypeManager();
  $rels = $etm->getStorage('group_relationship_type')
    ->loadByProperties(['content_plugin' => $plugin_type.':'.$entity->bundle()]);
  $group_types = $group_ids = $my_group_ids = [];
  foreach ($rels as $rel) {
    $group_types[] = $rel->getGroupType()->id();
  }
  $groups = $etm->getStorage('group')->loadByProperties(['type' => $group_types]);
  foreach ($groups as $group) {
    $group_ids[] = $group->id();
  }

  if(\Drupal::currentUser()->hasPermission('edit all smallads')) {
    $my_group_ids = \Drupal::entityQuery('group')->accessCheck(TRUE)->execute();
  }
  else {
    $my_memberships = \Drupal::service('group.membership_loader')->loadByUser(\Drupal::currentUser());
    foreach ($my_memberships as $mem) {
      $my_group_ids[] = $mem->getGroup()->id();
    }
  }
  return Group::loadMultiple(array_intersect($group_ids, $my_group_ids));
}


/**
 * Form submit callback for entity forms.
 * Add entities to, and remove from the groups.
 */
function cforge_group_entity_form_submit($form, $form_state) {
  $in_gids = $form_state->getValue('group_add');
  $entity = $form_state->getFormObject()->getEntity();
  $groups = Group::loadMultiple(array_keys($in_gids));
  foreach ($groups as $group) {
    $gid = $group->id();
    $relationships = $group->getRelationshipsByEntity($entity);
    $needed = in_array($group->id(), $in_gids);
    if ($needed and !$relationships) {
      $entity->save(); // must be saved to add to group. Don't know why it's not saved before now.
      $plugin_type = 'group_'.$entity->getEntityTypeId();
      $group->addRelationship($entity, $plugin_type.':'.$entity->bundle(), []); // group_smalladoffer
      $added[] = $group->label();
    }
    elseif (!$needed and $relationships) {
      // In case there are multiple relationships.
      foreach ($relationships as $rel) {
        $rel->delete();
      }
      $removed[] = $group->label();
    }
  }
  $bundle_entity_type = $entity->getEntityType()->getBundleEntityType();
  $bundle_label = \Drupal::entityTypeManager()->getStorage($bundle_entity_type)
    ->load($entity->bundle())
    ->label();
  if (isset($added)) {
    \Drupal::messenger()->addStatus(t('The @type was added to %groups', ['%groups' => implode(', ', $added), '@type' => $bundle_label]));
  }
  if (isset($removed)) {
    \Drupal::messenger()->addStatus(t('The @type was removed from %groups', ['%groups' => implode(', ', $removed), '@type' => $bundle_label]));
  }
}

/**
 * Implements hook_block_view_BASE_BLOCK_ID_alter().
 *
 * Change the title of the user block.
 */
function cforge_group_block_view_group_operations_alter(&$build, $block) {
  $build['#pre_render'][] = function ($array) {
    unset($array['content']['#type']);
    $array['content']['#theme'] = 'links';
    return $array;
  };
}
