<?php

/**
 * @file
 * Hooks for cforge_offline module.
 */

use Drupal\user\Entity\User;

/**
 * Implements hook_form_contact_personal_form_alter().
 *
 * Warn the user that the mail will go via a proxy member.
 */
function cforge_offline_form_contact_message_personal_form_alter(&$form, $form_state) {
  $nominees = User::load($form['recipient']['#value'])
    ->masquerade_nominees
    ->referencedEntities();
  if ($nominees) {
    $form['recipient']['#field_suffix'] = t('This member will be contacted through an intermediary.');
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Puts masquerade nominees under a checkbox so they only show when the box is
 * checked.
 */
function cforge_offline_form_user_form_alter(&$form, $form_state) {
  $formObject = $form_state->getFormObject();
  if ($formObject->getFormDisplay($form_state)->getComponent('masquerade_nominees')) {
    $form['offline'] = [
      '#title' => t('Non-internet member'),
      '#description' => t('Name a friend who can log in as you.'),
      '#type' => 'checkbox',
      '#default_value' => !$formObject->getEntity()->masquerade_nominees->isEmpty(),
      '#weight' => $form['masquerade_nominees']['#weight'] -1.
    ];

    $form['account']['mail']['#required'] = FALSE;
    $form['account']['mail']['#description'] .= ' ' . t('If you do not have an email account then you must create one. Another member can help you with this. Then you can nominate another member to manage your account for you.');
    // This runs AFTER masquerade nominate.
    $form['masquerade_nominees']['#states'] = [
      'visible' => [
        ':input[name="offline"]' => ['checked' => TRUE]
      ],
      'required' => [// Doesn't seem to work so see form validation callback
        ':input[name="offline"]' => ['checked' => TRUE]
      ]
    ];
    $form['#validate'][] = 'cforge_offline_user_validate';
  }
}

/**
 * Form validation callback.
 *
 * @see cforge_offline_form_user_form_alter
 */
function cforge_offline_user_validate($form, &$form_state) {
  if ($form_state->hasValue('offline') && !$form_state->getValue('offline')) {
    $form_state->setValue(['masquerade_nominees', 'target_id'], []);
  }
  else {
    if (!$form_state->getValue(['masquerade_nominees','target_id'])) {
      $form_state->setError(
        $form['masquerade_nominees']['widget']['target_id'],
        t('Offline users MUST nominate a helper')
      );
    }
  }
}

/**
 * Implements hook_ENTITY_TYPE_view_alter().
 *
 * Adds the contact card to the user profile page.
 */
function cforge_offline_user_view_alter(array &$build, $account, $display) {
  $build['#attached']['library'][] = 'cforge_offline/css';
  // Use the envelope image for postal users.
  if ($account->masquerade_nominees->referencedEntities()) {
    $build['address']['#attributes']['class'][] = 'offline';
    $build['address']['#attributes']['title'] = t("This member is 'offline' which means they have nominated another member to manage their online account.");
  }
  else {
    $build['address']['#attributes']['class'][] = 'not-offline';
  }
}

/**
 * Implements hook_mail_alter().
 *
 * Divert mail sent to offline users to their nominated users.
 *
 * @todo remove the checks from this function
 */
function cforge_offline_mail_alter(&$message) {
  if (!isset($message['params']['user'])) {
    return;
  }
  if ($nominees = $message['params']['user']->masquerade_nominees->referencedEntities()) {
    // Broadcast mails are not diverted, but blocked.
    if ($message['key'] == 'broadcast') {
      $message['send'] = FALSE;
      return;
    }
    // Divert the mail to nominees if there are any.
    foreach ($nominees as $proxy_recipient_account) {
      $to[] = $proxy_recipient_account->getEmail();
    }
    $message['to'] = implode(';', $to);
    $prefix = t('On behalf of @name', ['@name' => $proxy_recipient_account->getDisplayName()]);
    $message['subject'] = "[$prefix] " . $message['subject'];
  }
}

/**
 * Implements hook_views_pre_view().
 * Add the link to the printable displays.
 */
function cforge_offline_views_pre_view(\Drupal\views\ViewExecutable $view, $display_id, &$args) {
  $display = $view->getDisplay();
  if ($display->getPluginId() == 'page') {
    /** @var Drupal\views\Plugin\views\display\Page $display */
    $path = $display->getOption('path');
    if (in_array($path, ['members', 'transactions', 'offers', 'wants'])) {
      // This adds the footer to the default display, so the members
      // display is saved with non-default footer.
      $view->setHandler($display_id, 'footer', 'area_text_custom', [
        'id' => 'area_text_custom',
        'table' => 'views',
        'field' => 'area_text_custom',
        'group_type' => 'group',
        'plugin_id' => 'text_custom',
        'empty' => false,
        'content' => '<a href="/'.$path.'/print">'. t('Printable page') .'</a>'
      ]);
    }
  }
}

function cforge_offline_views_post_render(Drupal\views\ViewExecutable $view, &$output) {
  $path = $view->getDisplay()->getOption('path');
  if (in_array($path, ['members/print', 'transactions/print', 'offers/print', 'wants/print'])) {
    $output['#attached']['library'][] = 'cforge_offline/page';
  }
}

/**
 * Implements hook_migrate_prepare_row().
 */
function cforge_offline_migrate_d7_field_instance_prepare_row($row, $source, $migration) {
  if ($row->getSourceProperty('field_name') == 'account_offline') {
    throw new Drupal\migrate\MigrateSkipRowException ("Field account_offline is discontinued");
  }
}
function cforge_offline_migrate_d7_field_prepare_row($row, $source, $migration) {
  cforge_offline_migrate_d7_field_instance_prepare_row($row, $source, $migration);
}
