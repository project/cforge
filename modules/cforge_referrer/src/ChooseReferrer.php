<?php

namespace Drupal\cforge_referrer;

use Drupal\mcapi\Entity\Storage\WalletStorage;
use Drupal\mcapi\Entity\Transaction;
use Drupal\user\Entity\User;
use Drupal\Core\Entity\Element\EntityAutocomplete;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Logger\LoggerChannel;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form builder to choose who referred you to the site.
 */
class ChooseReferrer extends FormBase {

  protected $logger;
  protected $userData;

  public function __construct(LoggerChannel $logger_channel, $user_data) {
    $this->logger = $logger_channel;
    $this->userData = $user_data;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('logger.channel.cforge'),
      $container->get('user.data')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cforge_referrer_form';
  }

  /**
   * {@inheritDoc}
   *
   * @todo Exclude the current user from the selection handler.
   */
  public function buildForm(array $form, FormStateInterface $form_state, User $user = NULL) {
    $form_state->set('user', $user);
    $form['intro'] = [
      '#markup' => t(
        'Before you start filling in your profile, take a moment to reward whichever member introduced you to @site!',
        ['@site' => \Drupal::config('system.site')->get('name')]
      )
    ];
    $form['referrer'] = [
      '#title' => $this->t('Name of member'),
      '#type' => 'entity_autocomplete',
      '#target_type' => 'user',
      '#selection_handler' => 'alt_login:user',
      '#selection_settings' => [
        'include_anonymous' => 0,
        'filter' => [
          'type' => 'role',
          'role' => [RID_TRADER],
        ],
        'sort' => [
          'field' => 'name',
          'direction' => 'ASC',
        ],
      ],
      '#placeholder' => t('Name'),
      '#tags' => FALSE,
      '#size' => 30,
      '#element_validate' => ['::notSelf']
    ];
    $form['actions'] = [
      'submit' => [
        '#type' => 'submit',
        '#value' => $this->t('Reward this person'),
        '#states' => [
          'invisible' => [
            ':input[name="referrer"]' => ['value' => ''],
          ]
        ]
      ],
      'cancel' => [
        '#type' => 'submit',
        '#value' => $this->t('Nobody referred me'),
        '#states' => [
          'visible' => [
            ':input[name="referrer"]' => ['value' => ''],
          ]
        ],
        '#submit' => ['::noReferrer']
      ],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function notSelf(array &$element, FormStateInterface $form_state) {
    $referrer_uid = EntityAutocomplete::extractEntityIdFromAutocompleteInput($element['#value']);
    if ($referrer_uid == $form_state->get('user')->id()) {
      $form_state->setError($element, $this->t("Introducing yourself doesn't count!"));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $settings = \Drupal::config('cforge_referrer.settings');
    $referrer_uid = EntityAutocomplete::extractEntityIdFromAutocompleteInput($form_state->getValue('referrer'));
    $referrer_user = User::load($referrer_uid);
    $reward = Transaction::Create([
      'payer' => $settings->get('src_wallet'),
      'payee' => WalletStorage::firstWalletOfEntity($referrer_user),
      'workflow' => 'admin',
      'quant' => $settings->get('fee'),
      'description' => $this->t(
        'Reward for referring new member @mem',
        ['@mem' => $form_state->get('user')->label()]
      ),
      'category' => $settings->get('category')
    ]);
    $reward->save();
    $this->userData->set('cforge_referrer', $form_state->get('user')->id(), 'referred_by', $referrer_uid);
    $this->logger->info(
      'Referrer reward given to @name for referring @new_name',
      [
        '@name' => $form_state->get('user')->getDisplayName(),
        '@new_name' => $referrer_user->getDisplayName(),
      ]
    );
    \Drupal::messenger()->addStatus(
      $this->t(
        '@name has been given @amount for referring you!',
        [
          '@name' => $referrer_user->toLink()->toString(),
          '@amount' => $reward->worth,
        ]
      )
    );
  }

  /**
   * Submit callback
   */
  function noReferrer($form, $form_state) {
    $this->userData->delete('cforge_referrer', $form_state->get('user')->id(), 'referred_by');
  }
}
