Docs module creates a new content type, document, which takes attached files, and a new vocabulary for it.
Documents can only be viewed by authenticated users.
A page view shows documents, by category
