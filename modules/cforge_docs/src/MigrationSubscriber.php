<?php

namespace Drupal\cforge_docs;

use Drupal\migrate\MigrateSkipRowException;
use Drupal\migrate\Event\MigratePreRowSaveEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Migration modifications
 */
class MigrationSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() : array {
    return [
      'migrate.pre_row_save' => [['migratePreRowSave']]
    ];
  }

  /**
   * @param Drupal\migrate\Event\MigratePreRowSaveEvent $event
   */
  public function migratePreRowSave(MigratePreRowSaveEvent $event) {
    $row = $event->getRow();
    $migration = $event->getMigration();

    // Copy the terms to the new vocab
    if ($migration->id() == 'd7_taxonomy_term:cforge_docs_categories') {
      $row->setDestinationProperty('vid', 'binders');
    }

    if ($migration->id() == 'd7_taxonomy_vocabulary') {
      if ($row->getSourceProperty('machine_name') == 'cforge_docs_categories') {
        throw new MigrateSkipRowException();
      }
    }

    // Copy the term references to the new field
    if ($event->getMigration()->id() == 'd7_node:document') {
      $row->setDestinationProperty('terms', $row->getDestinationProperty('cforge_docs_categories'));
    }

    // Don't copy the categories field or field instance
    if ($migration->id() == 'd7_field' or $migration->id() == 'd7_field_instance') {
      if ($row->getSourceProperty('field_name') == 'cforge_docs_categories') {
        throw new MigrateSkipRowException('Binders field is already installed.');
      }
    }

    // Don't copy the node_type
    if ($migration->id() == 'd7_node_type') {
      if ($row->getSourceProperty('type') == 'document') {
        throw new MigrateSkipRowException("Document node type is already installed");
      }
    }

  }

}
