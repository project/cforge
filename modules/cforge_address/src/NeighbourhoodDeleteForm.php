<?php

namespace Drupal\cforge_address;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityDeleteForm;



/**
 * Delete a neighbourhood, reassigning users to another neighbourhood.
 */
class NeighbourhoodDeleteForm extends EntityDeleteForm {


  public function getDescription() {
    return $this->t('Reassign any current inhabitants to this neighbourhood. You might want to create that neighbourhood first!');
  }


  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $form['fallback'] = [
      '#title' => $this->t('Fallback'),
      '#type' => 'cf_neighbourhood_select',
      '#exclude' => [$this->getEntity()->label()]
    ];

    $form_state->set('orig_name', $this->entity->label());
    return $form;
  }

  /**
   * {@inheritdoc}
   */

  public function submitForm(array &$form, FormStateInterface $form_state) {
    cf_change_neighbourhood($form_state->get('orig_name'), $form_state->getValue('fallback'));
    $this->getEntity()->delete();
    $this->messenger()->addStatus($this->getDeletionMessage());
    $form_state->setRedirectUrl($this->getCancelUrl());
    $this->logDeletionMessage();
  }



}
