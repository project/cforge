<?php

namespace Drupal\cforge_address\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Language\LanguageInterface;

/**
 * Defines the neighbourhood entity.
 *
 * @ConfigEntityType(
 *   id = "neighbourhood",
 *   label = @Translation("Neighbourhood"),
 *   handlers = {
 *     "access" = "Drupal\Core\Entity\EntityAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\cforge_address\NeighbourhoodForm",
 *       "edit" = "Drupal\cforge_address\NeighbourhoodForm",
 *       "delete" = "Drupal\cforge_address\NeighbourhoodDeleteForm",
 *     },
 *     "list_builder" = "Drupal\cforge_address\NeighbourhoodListBuilder"
 *   },
 *   admin_permission = "administer site configuration",
 *   config_prefix = "hood",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label"
 *   },
 *   links = {
 *     "delete-form" = "/admin/structure/neighbourhoods/{neighbourhood}/delete",
 *     "edit-form" = "/admin/structure/neighbourhoods/{neighbourhood}",
 *     "add-form" = "/admin/structure/neighbourhoods/add",
 *     "collection" = "/admin/structure/neighbourhoods",
 *   },
 *   config_export = {
 *     "id",
 *     "label"
 *   }
 * )
 */

class Neighbourhood extends ConfigEntityBase {

  /**
   * The unique id
   *
   * @var string
   */
  protected $id;

  /**
   * The neighbourhood name.
   *
   * @var string
   */
  protected $label;

  /**
   * {@inheritDoc}
   */
  static function create(array $values = []) {
    if (isset($values['label']) and !isset($values['id'])) {
      $values['id'] = self::generateId($values['label']);
    }
    return parent::create($values);
  }

  /**
   * {@inheritDoc}
   */
  public static function sort(ConfigEntityInterface $a, ConfigEntityInterface $b) {
    return strnatcasecmp($a->label(), $b->label());
  }

  public static function generateId(string $name) : string {
    $transliteration = \Drupal::transliteration();
    $all = \Drupal::entityTypeManager()->getStorage('neighbourhood')->loadMultiple();
    $suggestion = preg_replace('@[^A-Za-z0-9_.]+@', '', $name);
    $suggestion = $transliteration->removeDiacritics($suggestion);
    $suggestion = $transliteration->transliterate($suggestion, LanguageInterface::LANGCODE_SITE_DEFAULT, '', 8);
    $suggestion = mb_strtolower($suggestion);
    $i=0;
    while (in_array($suggestion, array_keys($all))) {
      $i++;
      $suffix = '_'.$i;
      $suggestion = substr_replace($suggestion, $suffix, strlen($suggestion) - strlen($suffix));
    }
    return $suggestion;
  }


  static function import(array $names) {
    foreach (static::Loadmultiple() as $hood) {
      $hood->delete();
    }
    foreach ($names as $label) {
      $n = self::Create(['label' => trim($label)]);
      $n->save();
      $created[] = $n;
    }
    return $created;
  }
}
