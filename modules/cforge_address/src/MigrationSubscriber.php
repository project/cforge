<?php

namespace Drupal\cforge_address;

use Drupal\migrate\Event\MigrateImportEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\cforge_address\Entity\Neighbourhood;

/**
 * Migration modifications
 *
 * Note the order of events in MigrateExecutable::import
 * Dispatch PRE_IMPORT
 *  $source->next
 *  $source->prepareRow
 *    hook_migrate_prepare_row
 *    hook_migrate_NAME_prepare_row
 *  while
 *    processRow
 *    Dispatch PRE_ROW_SAVE
 *    Dispatch POST_ROW_SAVE
 *  Dispatch POST_IMPORT
 *
 */
class MigrationSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() : array {
    return [
      'migrate.post_import' => [['migratePostImport']]
    ];
  }


  /**
   * @param Drupal\migrate\Event\MigratePreRowSaveEvent $event
   */
  function migratePostImport(MigrateImportEvent $event) {
    if ($event->getMigration()->id() == 'd7_cf_neighbourhoods') {
      $all = Neighbourhood::loadMultiple();
      if (count($all) < 2) {
        \Drupal::service('module_installer')->uninstall(['cforge_address']);
      }
    }
  }

}
