<?php

namespace Drupal\cforge_address;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Config\Entity\ConfigEntityListBuilder;

/**
 * Defines the default class to build a listing of configuration entities.
 *
 * @ingroup entity_api
 */
class NeighbourhoodListBuilder extends ConfigEntityListBuilder {


  /**
   * {@inheritDoc}
   */
  public function buildHeader() {
    $row['name'] = $this->t('neighbourhood');
    $row['operations'] = $this->t('Operations');
    return $row;
  }

  /**
   * {@inheritDoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['name'] = $entity->label();
    $row['operations']['data'] = $this->buildOperations($entity);
    return $row;
  }
}
