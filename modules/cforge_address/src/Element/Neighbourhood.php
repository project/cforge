<?php

namespace Drupal\cforge_address\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\Select;
use Drupal\Core\Render\Attribute\FormElement;

/**
 * Tweaks the default address form element
 */
#[FormElement('cf_neighbourhood_select')]
class Neighbourhood extends Select {

  /**
   * {@inheritDoc}
   */
  public function getInfo() {
    $info = parent::getInfo();
    $info['#required'] = TRUE;
    return $info;
  }

  /**
   * {@inheritDoc}
   */
  public static function processSelect(&$element, FormStateInterface $form_state, &$complete_form) {
    $element['#options'] = cforge_list_neighbourhoods();

    if (!isset($element['#default_value'])) {
      $element['#empty_option'] = '-- '.t('Choose').' --';
    }
    unset($element['#size']);
    return parent::processSelect($element, $form_state, $complete_form);
  }


}
