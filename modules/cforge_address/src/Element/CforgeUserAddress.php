<?php

namespace Drupal\cforge_address\Element;

use Drupal\Core\Form\FormStateInterface;

/**
 * Tweaks the CommerceGuys address form element for local use with neighbourhoods
 *
 * @FormElement("cforge_user_address")
 */
class CforgeUserAddress extends \Drupal\address\Element\Address {

  /**
   * Override the address widget assuming there is no choice of country and that
   * neighbourhood is a dropdown. Restrict the available countries to those in
   * config.
   */
  public static function processAddress(array &$element, FormStateInterface $form_state, array &$complete_form) {
    $element = parent::processAddress($element, $form_state, $complete_form);
    // Change the widget for the neighbourhood
    $element['dependent_locality']['#type'] = 'cf_neighbourhood_select';
    $element['#attached']['library'][] = 'cforge_address/address_css';
    return $element;
  }
}