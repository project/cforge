<?php

namespace Drupal\cforge_address;

use Drupal\address\Event\AddressEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\address\Event\AddressFormatEvent;

/**
 * Ensure the dependent_locality address property is required in every addressformat
 */
class AddressSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() : array {
    $events[AddressEvents::ADDRESS_FORMAT] = [['rewriteAddress']];
    return $events;
  }

  /**
   * Ensure every address format has %dependentLocality, sticking it on the end
   * if necessary, that that field is required, and called neighbourhood
   */
  public function rewriteAddress(AddressFormatEvent $event) {
    $address_format = $event->getDefinition();
    // Ensure every country's format contrains the 'neighbourhood' field.
    if (!strpos($address_format['format'], '%dependentLocality')) {
      $address_format['format'] .= "\n%dependentLocality";
    }
    $address_format['required_fields'][] = 'dependentLocality';
    $address_format['format'] = str_replace("%addressLine3\n", '', $address_format['format']);

    if ($address_format['country_code'] == 'FR') {
      $address_format['format'] = str_replace("%sortingCode", '', $address_format['format']);
    }

    // Redefine the format for France.
    if ($address_format['country_code'] == 'FR') {
      $address_format['format'] = "%addressLine1
%addressLine2
%dependentLocality
%postalCode %locality";
      $address_format['uppercase_fields'][] = 'postalCode';
      $address_format['administrative_area_type'] = 'department';
    }
    $event->setDefinition($address_format);
  }

}
