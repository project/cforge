<?php

namespace Drupal\cforge_address\Plugin\migrate\destination;

use Drupal\cforge_address\Entity\Neighbourhood;
use Drupal\migrate\Row;
use Drupal\migrate\Plugin\migrate\destination\EntityConfigBase;
use Drupal\migrate\Attribute\MigrateDestination;

/**
 * Provides variable destination plugin.
 */
#[MigrateDestination('cf_neighbourhoods')]
class Neighbourhoods extends EntityConfigBase {

  /**
    * {@inheritdoc}
    */
  public function import(Row $row, array $old_destination_id_values = []) {
    $hood_list = $row->getSourceProperty('cforge_neighbourhoods');
    if (empty($hood_list)) {
      \Drupal::messenger()->addWarning('No Neighbourhoods found to migrate: '.implode('; ', $row->getSource()));
      return [];
    }

    $hoods = Neighbourhood::import(explode("\n", $hood_list));
    //just return one nid because only one is expected
    return [reset($hoods)->id()];
  }

  /**
   * {@inheritDoc}
   */
  public function getIds() {
    return [
      'id' => [
        'type' => 'string',
        'max_length' => 8,
        'is_ascii' => TRUE,
      ],
    ];
  }


  protected static function getEntityTypeId($plugin_id) {
    return 'neighbourhood';
  }


}
