<?php

namespace Drupal\cforge_address;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Transliteration\TransliterationInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base form controller for category edit forms.
 */
class NeighbourhoodForm extends EntityForm {

  /**
   * @param \Drupal\Component\Transliteration\TransliterationInterface $transliteration
   */
  function __construct(TransliterationInterface $transliteration) {
    $this->transliteration = $transliteration;
  }

  public static function create(ContainerInterface $container) {
    return new static(
       $container->get('transliteration')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $form_state->set('orig_name', $this->entity->label());

    $form['label'] = [
      '#title' => t('Label'),
      '#type' => 'textfield',
      '#maxlength' => 16,
      '#default_value' => $this->entity->label(),
      '#required' => TRUE,
    ];
    $form['id'] = array(
      '#type' => 'machine_name',
      '#default_value' => $this->entity->id(),
      '#machine_name' => array(
        'exists' => '\Drupal\cforge_address\Entity\Neighbourhood::load',
      ),
      '#maxlength' => 8,
      '#access' => $this->entity->isNew(),
    );

    $form['actions'] = [
      '#type' => 'actions',
      'submit' => [
        '#type' => 'submit',
        '#value' => t('Save'),
      ]
    ];

    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    cf_change_neighbourhood($form_state->get('orig_name'), $form_state->getValue('label'));
    // Remove button and internal Form API values from submitted values.
    $form_state->cleanValues();
    $this->entity = $this->buildEntity($form, $form_state);

    $form_state->setRedirectUrl($this->entity->toUrl('collection'));
  }



}

