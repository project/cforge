<?php

namespace Drupal\cforge_events;

use Drupal\migrate\MigrateSkipRowException;
use Drupal\migrate\Event\MigratePreRowSaveEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;


/**
 * Migration modifications
 */
class MigrationSubscriber implements EventSubscriberInterface {


  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() : array {
    return [
      'migrate.pre_row_save' => [['migratePreRowSave']],
    ];
  }


  /**
   * @param Drupal\migrate\Event\MigratePreRowSaveEvent $event
   */
  public function migratePreRowSave(MigratePreRowSaveEvent $event) {
    $row = $event->getRow();
    $migration = $event->getMigration();

    if ($migration->id() == 'd7_field' or $migration->id() == 'd7_field_instance') {
      if ($row->getSourceProperty('field_name') == 'event_date') {
        throw new MigrateSkipRowException('Event date field is already installed.');
      }
    }

    if ($migration->id() == 'd7_node_type') {
      if ($row->getSourceProperty('type') == 'event') {
        throw new MigrateSkipRowException("Event node type is already installed");
      }
    }

    if($migration->id() == 'd7_comment_type' or $migration->id() == 'd7_comment_field') {
      if ($row->getSourceProperty('type') == 'event') {
        throw new MigrateSkipRowException('Comments for events already exist');
      }
    }
  }



}

