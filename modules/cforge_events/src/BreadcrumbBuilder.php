<?php

namespace Drupal\cforge_events;

use Drupal\Core\Link;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Cache\CacheableMetadata;

/**
 * Defines a class to build path-based breadcrumbs.
 *
 * @see \Drupal\Core\Breadcrumb\BreadcrumbBuilderInterface
 *
 * @todo inject \Drupal::service('datetime.time')
 */
class BreadcrumbBuilder extends \Drupal\cforge\NodeBreadcrumbBuilder {
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match, ?CacheableMetadata $cacheable_metadata = null) {
    if ($route_match->getParameter('node')) {
      return $route_match->getParameter('node')->getType() == 'event';
    }
    elseif (str_starts_with($route_match->getRouteName(), 'view.events')) {
      return TRUE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function build(RouteMatchInterface $route_match) {
    $links = [
      Link::createFromRoute($this->t('Events'), 'view.events.page')
    ];
    if ($node = $route_match->getParameter('node')) {
      $now = \Drupal::service('datetime.time')->getRequestTime();
      if (strtotime($node->when->value) < $now) {
        $links[] = Link::createFromRoute($this->t('Previous'), 'view.events.page', ['arg_0' => time()]);
      }
      $links[] =  Link::createFromRoute($node->label(), 'entity.node.canonical', ['node' => $node->id()]);
      if ($route_match->getRouteName() == 'entity.node.edit_form') {
        $links[] = Link::createFromRoute($this->t('Edit'), '<current>');
      }
    }
    return $this->breadcrumbs($links);
  }

}
