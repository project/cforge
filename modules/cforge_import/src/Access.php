<?php

namespace Drupal\cforge_import;

use Drupal\Core\Session\AccountInterface;

/**
 * Access check.
 *
 * @deprecated
 */
class Access {

  /**
   * Grants access if the site unlaunched flag exists.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   */
  public function unlaunched(AccountInterface $account) {
    return AccessResult::allowedIf(\Drupal::config('cforge.settings')->get('unlaunched') &&
        \Drupal::moduleHandler()->moduleExists('mcapi_import'));
  }
}

