<?php

namespace Drupal\cforge_import\Plugin\CsvParser;
/**
 * Interface for CsvParser plugins.
 */
interface CsvParserInterface {

  /**
   * Get the names & descriptions of the expected CSV columns.
   *
   * @return array
   *   Descriptions, keyed by the csv column name.
   */
  public function columns();

  /**
   * Delete all the existing entities.
   */
  public static function deleteAll();

  /**
   * Convert the csv rows into entities and save them.
   */
  public static function saveEntities(string $plugin_id, array $rows, bool $test, array &$sandbox = []) : void ;

  /**
   * Convert one row into a content entity.
   */
  public function buildEntity(array $fields);

  /**
   * Chunk the csv rows and set them into a batch.
   */
  public function makeBatch($rows, $delete = FALSE, $test = TRUE);

  /**
   * Determine whether the system is ready to import with this plugin.
   */
  public function ready() : bool;

}
