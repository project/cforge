<?php

namespace Drupal\cforge_import\Plugin\CsvParser;

use Drupal\user\Entity\Role;
use Drupal\field\Entity\FieldConfig;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base plugin for importing users.
 */
abstract class Users extends ImportBase {

  protected $siteSettings;
  private $defaultCountryCode;

  /**
   * Constructor.
   */
  public function __construct($configuration, $plugin_id, $plugin_definition, $logger_channel, $config_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $logger_channel);
    $this->siteSettings = $config_factory->get('system_site');
    $this->defaultCountryCode = $config_factory->get('system.date')->get('country.default');
    $this->roles = Role::loadMultiple();
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.channel.cforge'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritDoc}
   */
  public static function deleteAll() {
    foreach (\Drupal::entityTypeManager()->getStorage('user')->loadMultiple() as $entity) {
      if ($entity->id() > 1) {
        $entity->delete();
      }
    }
  }

  /**
   * {@inheritDoc}
   */
  protected function getDefaults() {
    return ['status' => TRUE];
  }

  /**
   * {@inheritDoc}
   */
  public function columns() {
    $fields = [
      'uid' => t('Unique user integer ID (No. 1 is reserved)'),
      'name' => t('user/login name'),
      'mail' => t('email address*'),
      'roles' => t('machine name of the role, e.g. trader, committee'),
      'created' => t('created date in any parsable php strtotime format'),
      'changed' => t('last time the account was saved'),
      'access' => t('last login time in any parsable php strtotime format')
    ];
    return $fields;
  }

  /**
   * {@inheritDoc}
   */
  public function makeBatch($rows, $delete = FALSE, $test = TRUE) {
    // Create the user picture directory if needed
    $field = FieldConfig::load('user.user.user_picture');
    $dir = 'public://' . $field->getSetting('file_directory');
    if (!file_exists($dir)) {
      \Drupal::service('file_system')->mkdir($dir, 509, 8);
    }
    $batch = parent::makeBatch($rows, $delete, $test);
    $batch['title'] = 'Importing users';
    return $batch;
  }

  public function buildEntity(array $fields) {
    parent::buildEntity($fields);
    if (empty($field['access'])) {
      $this->entity->access->value = $this->entity->changed->value;
    }
    $this->entity->address->country_code = $this->defaultCountryCode;
    $lat = $this->entity->coordinates->lat;
    $lon = $this->entity->coordinates->lon;
    $this->entity->coordinates->value = "POINT ($lon $lat)";
  }

  /**
   * Preprocessing for csv field 'roles'.
   */
  protected function rolesProcess($val) {
    foreach (explode(',', $val) as $role_name) {
      $role_name = trim($role_name);
      if (isset($this->roles[$role_name])) {
        $this->entity->addRole($role_name);
      }
      else foreach($this->roles as $rname => $real_role) {
        if ($real_role->label() == $role_name) {
          $this->entity->addRole($rname);
        }
      }
    }
  }

  /**
   * Preprocessing for csv field 'mail'.
   */
  protected function mailProcess($val) {
    $val = trim(strtolower($val));
    if (empty($val) or !preg_match('/^.+\@\S+\.\S+$/', $val)) {
      $sitemail = \Drupal::config('system.site')->get('mail');
      $suffix = substr($sitemail, strpos($sitemail, '@'));
      if (!$suffix) {
        throw new Exception('Site mail cannot be used for creating dummy emails: ' . $sitemail);
      }
      $val = $this->fields['name'] . $suffix;
    }
    $this->entity->setEmail($val);
    $this->entity->set('init', $val);

    // If no username is given, set it to the email.
    if (empty($this->fields['name'])) {
      $this->entity->setUsername($val);
    }
  }

  /**
   * {@inheritDoc}
   */
  public function ready() : bool {
    return TRUE;
  }

}
