<?php

namespace Drupal\cforge_import;

use Drupal\cforge\Forms\MassContact;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form builder to launch the a site.
 */
class Launch extends MassContact {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cforge_launch_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildform(array $form, FormStateInterface $form_state) {
    \Drupal::messenger()->addStatus(t('This page can be used once only. It will mail every user optionally with a password reset link, notify CES to divert all traffic to this site.'));
    $form = parent::buildForm($form, $form_state);
    $form['role_id']['#type'] = 'value';
    $form['role_id']['#value'] = 'never';
    $form['body']['#required'] = TRUE;
    $form['body']['#element_validate'][] = [get_class($this), 'checkLoginLink'];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function checkLoginLink($element, $form_state) {
    if (!strpos($form_state->get('body'), '[user:one-time-login-url]')) {
      $form_state->setError($element, 'The body must include the token [user:one-time-login-url]');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submit($form, FormStateInterface $form_state) {
    // Send the mail to all users.
    if ($form_state->getValue('body')) {
      parent::submit($form, $form_state);
    }
    // If this is a test don't follow through!
    if ($form_state->getValue('scope') == static::SCOPE_ME) {
      return;
    }
    \Drupal::service('module_installer')->uninstall(['cforge_import']);
  }

}
