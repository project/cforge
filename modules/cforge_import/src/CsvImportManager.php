<?php

namespace Drupal\cforge_import;

use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Plugin manager for csv imports.
 */
class CsvImportManager extends DefaultPluginManager {

  protected $entityTypeManager;
  protected $tempStore;

  /**
   * Constuctor.
   */
  public function __construct($namespaces, $module_handler, $entity_type_manager) {
    parent::__construct(
      'Plugin/CsvParser',
      $namespaces,
      $module_handler,
      '\Drupal\cforge_import\Plugin\CsvParser\CsvParserInterface'
    );
    // Only needed sometimes.
    $this->entityTypeManager = $entity_type_manager;
  }

}
