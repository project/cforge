<?php

namespace Drupal\cforge_import\Annotation;
use Drupal\Component\Annotation\Plugin;

/**
 * Annotations for the csv import plugin.
 *
 * @Annotation
 */
class CsvParser extends Plugin {

  public $id;

  public $label;

}
