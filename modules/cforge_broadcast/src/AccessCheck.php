<?php

namespace Drupal\cforge_broadcast;

use Drupal\Core\Routing\Access\AccessInterface;
use Symfony\Component\Routing\Route;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Check if the user can broadcast the node or smallad.
 * @todo inject keyvalue, entityTypemanager
 */
class AccessCheck implements AccessInterface {

  const NODE_TYPES = ['story', 'document', 'event'];

  /**
   * Allowed for admins or if the site allows users to broadcast AND if it has not been broadcast before.
   */
  public function access(Route $route, RouteMatchInterface $route_match, AccountInterface $account) {
    $params = $route_match->getParameters()->all();
    $entity = reset($params);

    if ($entity->getEntityTypeId() == 'node' and !in_array($entity->bundle(), self::NODE_TYPES)) {
      return AccessResult::forbidden();
    }

    $already = (array)\Drupal::keyValue('cforge_broadcast')->get($entity->getEntityTypeId());
    if (in_array($entity->id(), $already)) {
      return AccessResult::forbidden();
    }

    if ($account->hasPermission('broadcast any content')) {
      return AccessResult::allowed();
    }

    if ($entity->getOwnerId() == $account->id() && $account->hasPermission('broadcast own content')) {
      return AccessResult::allowed();
    }

    return AccessResult::forbidden();
  }
}
