<?php

namespace Drupal\cforge_broadcast\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\user\Entity\User;
use Drupal\Core\Queue\Attribute\QueueWorker;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Broadcast one mail
 */
#[QueueWorker(
  id: 'cforge_autonews',
  title: new TranslatableMarkup('Broadcast one mail'),
  cron: ['time' => 60]
)]
class AutoNews extends QueueWorkerBase {

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $account = User::load($data['uid']);
    $params = [
      'user' => $account,
      'since' => $data['since'],
    ];

    \Drupal::service('plugin.manager.mail')
      ->mail('cforge_broadcast', 'autonews', $account->mail, $account->getPreferredLangcode(), $params);

  }

}
