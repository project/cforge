<?php

namespace Drupal\cforge_broadcast;

use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\User;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\group\Entity\GroupRelationship;
use Drupal\group\Entity\Group;
use Drupal\Core\Entity\ContentEntityForm;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form builder for broadcasting nodes and smallads.
 */
class SendForm extends ContentEntityForm {

  private $keyValueStore;
  private $routeBuilder;

  public function __construct($entity_repository, $entity_type_bundle_info, $time, $key_value_store, $route_builder) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);
    $this->keyValueStore = $key_value_store->get('cforge_broadcast');
    $this->routeBuilder = $route_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('keyvalue'),
      $container->get('router.builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cforge_broadcast_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::init($form_state);
    $form['audience'] = [
      '#title' => t('Email the following users'),
      '#type' => 'radios',
      '#options' => $this->getOptions(),
      '#default_value' => 'myself',
      '#weight' => 1
    ];
    $form['warning'] = [
      '#type' => 'item',
      '#markup' => t('This item can only be broadcast once!'),
      '#weight' => 9
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => t('Send'),
      '#attributes' => [
        'onclick' => 'if(!confirm("' . (string) $this->t('Are you sure you want to email this?') . '")){return false;}',
      ],
      '#weight' => 10
    ];

    return $form;
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
    // There's nothing to validate and these non-entity fields break the endity validation.
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $scope = $form_state->getValue('audience');
    if ($scope == 'myself') {
      $uids = [$this->currentUser()->id()];
    }
    elseif ($scope == 'neighbourhood') {
      $account = User::load($this->currentUser()->id());
      $address = $account->address->getValue();
      $uids = cforge_neighbour_uids($address[0]['dependent_locality']);
    }
    elseif ($scope == 'site') {
      $uids = \Drupal::entityQuery('user')->accessCheck(TRUE)
        ->condition('status', 1)
        ->condition('uid', 1, '<>')
        ->execute();
    }
    elseif (substr($scope, 0, 6) == 'group:') {
      [,$gid] = explode(':', $scope);
      foreach (Group::load($gid)->getmembers() as $rel) {
        $uids[] = $rel->getUser()->id();
      }
    }
    else {
      throw new \Exception('Unknown broadcast audience: '.$form_state->getValue('scope'));
    }

    $entity = $form_state->getFormObject()->getEntity();
    if ($form_state->getValue('scope') <> 'myself') {
      if ($this->config('cforge_broadcast.settings')->get('optout')) {
        $opted_out = \Drupal::service('user.data')->get('cforge_broadcast', NULL, 'optout');
        $uids = array_diff($uids, array_keys(array_filter($opted_out)));
      }
      // Prevent the entity from being broadcast again.
      $this->markAsBroadcast($entity);
    }

    cforge_batch_mail(
      'cforge_broadcast',
      'broadcast',
      $uids,
      ['entity' => $entity]
    );
    $form_state->setRedirect('entity.'.$entity->getEntityTypeId().'.canonical', [$entity->getEntityTypeId() => $entity->id()]);
  }

  /**
   * Get the broadcast scopes.
   * @return array
   * @todo add groups.
   */
  protected function getOptions() : array {
    $options = ['myself' => t('Test to myself')];
    $user = User::load($this->currentUser()->id());

    if ($user->address->dependent_locality) {
      $options['neighbourhood'] = $user->address->dependent_locality;
    }
    if (\Drupal::moduleHandler()->moduleExists('cforge_group') and \Drupal::currentUser()->hasPermisiion('broadcast group')) {
      foreach (GroupRelationship::loadByEntity($this->entity) as $relationship) {
        $group = $relationship->getGroup();
        $options['group:'.$group->id()] = t('Group: @groupname', ['@groupname' => $group->label()]);
      }
    }
    $options['site'] = $this->t('Everyone in @site', ['@site' =>  $this->config('system.site')->get('name')]);
    return $options;
  }

  /**
  * Add the broadcasted entity to the list of already broadcast entities.
  *
  * @param ContentEntityInterface $entity
  *   The entity which has been broadcast.
  */
  protected function markAsBroadcast(ContentEntityInterface $entity) {
    $already = $this->keyValueStore->get($entity->getEntityTypeId());
    $already[] = $entity->id();
    $this->keyValueStore->set($entity->getEntityTypeId(), $already);
    $this->routeBuilder->rebuild();
  }

}
