<?php

/**
 * @file
 * Hooks for cforge_broadcast module.
 */

use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\node\Entity\NodeType;
use Drupal\Core\Render\Markup;

const CFORGE_BROADCAST_ME = 0;
const CFORGE_BROADCAST_NEIGHBOURHOOD = 1;
const CFORGE_BROADCAST_OGS = 2;
const CFORGE_BROADCAST_SITE = 3;

/**
 * Implements hook_form_alter().
 *
 * Add settings to the main cforge settings page.
 */
function cforge_broadcast_form_cforge_settings_alter(&$form, &$form_state) {
  $config = \Drupal::config('cforge_broadcast.settings');
  $form['mail']['optout'] = [
    '#title' => t('Broadcast opt-out'),
    '#description' => t('Give each user a setting to opt-out of broadcasts'),
    '#type' => 'checkbox',
    '#default_value' => $config->get('optout'),
    '#weight' => 2
  ];
  $form['#submit'][] = 'cforge_broadcast_settings_submit';
}

/**
 * Form submit callback.
 *
 * Save the broadcast settings.
 */
function cforge_broadcast_settings_submit($form, $form_state) {
  $config = \Drupal::configFactory()->getEditable('cforge_broadcast.settings');
  $config
    ->set('optout', $form_state->getValue('optout'))
    ->save();
}

/**
 * Implements hook_user_form_alter().
 *
 * Allows the user to opt out of broadcast if global settings allow.
 */
function cforge_broadcast_form_user_form_alter(&$form, $form_state) {
  $form_object = $form_state->getFormObject();
  if (\Drupal::config('cforge_broadcast.settings')->get('optout')) {
    if ($info = $form_object->getFormDisplay($form_state)->getComponent('contact')) {
      $form['optout'] = [
        '#title' => t("Opt-out of 'broadcast' messages"),
        '#description' => t("Do not send me one-off 'broadcast' messages"),
        '#type' => 'checkbox',
        '#default_value' => \Drupal::service('user.data')
          ->get('cforge_broadcast', $form_object->getEntity()->id(), 'optout'),
        '#weight' => $info['weight'],
      ];
      $form['actions']['submit']['#submit'][] = 'cforge_optout_user_submit';
    }
  }
}

/**
 * Submit callback for user form.
 */
function cforge_optout_user_submit($form, $form_state) {
  \Drupal::service('user.data')
    ->set(
      'cforge_broadcast',
      $form_state->getFormObject()->getEntity()->id(),
      'optout',
      $form_state->getValue('optout')
    );
}

function cforge_broadcast_entity_type_build(&$entity_types) {
  $entity_types['node']->setFormClass('broadcast', 'Drupal\\cforge_broadcast\\SendForm');
  $entity_types['smallad']->setFormClass('broadcast', 'Drupal\\cforge_broadcast\\SendForm');
}

/**
 * Implements hook_mail().
 *
 * Only one $key possible, which is broadcast.
 */
function cforge_broadcast_mail($key, &$message, $params) {
  $entity = $params['entity'];
  $params[$entity->getEntityTypeId()] = $entity;
  unset($params['entity']);
  if (!isset($params['user'])) {
    \Drupal::service('logger.channel.cforge')->error('No user parameter in broadcast mail');
  }
  $sender = $entity->getOwner();
  $message['subject'] = t(
    '%site_name: %entity_label',
    [
      '%site_name' => \Drupal::Config('system.site')->get('name'),
      '%entity_label' => $entity->label(),
    ]
  );
  $bundleFieldName = $entity->getEntityType()->getKey('bundle');
  $message['body'][] = t(
    '@name [<a href=":contact-url">contact</a>] posted a new @type.',
    [
      '@name' => $sender->getDisplayName(),
      ':contact-url' => Url::fromRoute(
        'entity.user.contact_form',
        ['user' => $sender->id()]
      )->toString(),
      '@type' => $entity->{$bundleFieldName}->entity->label(),
    ]
  );

  $renderable = \Drupal::entityTypeManager()
    ->getViewBuilder($entity->getEntityTypeId())
    ->view($entity, 'default');
  unset($renderable['comments']);

  $message['body'][] = \Drupal::service('renderer')->render($renderable);

  // Add a paragraph about opting out if settings allow.
  if (\Drupal::config('cforge_broadcast.settings')->get('optout')) {
    $message['body'][] = Link::fromTextAndUrl(
      t("Opt-out of 'broadcast' messages"),
      Url::fromRoute(
        'entity.user.edit_form',
        ['user' => $params['user']->id()],
        ['absolute' => TRUE]
      )
    )->toString();
  }
  $message['headers']['From'] = $sender->getEmail();

  if ($entity->getEntityTypeId() == 'smallad' and property_exists($entity, 'image')) {
    mail('matslats@fastmail.com', 'broadcast attachments', 'somebody tried to email an image attachment');
  }
  foreach (['attached', 'image', 'attached_private'] as $field_name) {
    if ($entity->hasField($field_name) and !$entity->get($field_name)->isEmpty()) {
      $file = $entity->get($field_name)->entity;
      // Not tested
      $params['attachment'][] = [
        'file' => $file,
        'filename' => $file->getFilename(),
        'filemime' => $file->getMimeType(),
      ];
    }
  }
}

/**
 * Implements hook_mail_alter().
 * Prevent user 1 from receiving broadcast mails.
 */
function cforge_broadcast_mail_alter($message) {
  if ($message['id'] == 'cforge_broadcast_broadcast') {
    if ($message['to'] == User::load(1)->getEmail()) {
      $message['send'] = FALSE;
    }
  }
}

/**
 * Put the broadcasted icon in the title block;
 */
function cforge_broadcast_preprocess_block(&$vars) {
  // Check if the current route is a node view.
  if ($vars['plugin_id'] == 'page_title_block') {
    $routeMatch = \Drupal::routeMatch();
    if (str_starts_with($routeMatch->getRouteName(), 'entity.') and $params = $routeMatch->getParameters()->all()) {
      $entity_type = key($params);
      $entity = current($params);
      $already = \Drupal::keyValue('cforge_broadcast')->get($entity_type) ?? [];
      if (is_object($entity) and in_array($entity->id(), $already)) {
        $vars['#attached']['library'][] = 'cforge_broadcast/broadcasted';
        $bundle_name = NodeType::load($entity->bundle())->label();
        $addendum = '<div class="broadcasted" title = "'.t('This @type has been sent to members.', ['@type' => $bundle_name]).'"></div>';
        $vars['content']['#title'] = Markup::create($vars['content']['#title'] . $addendum);
      }
    }
  }
}
