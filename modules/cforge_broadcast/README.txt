Broadcast module allows permitted users to email a node to other users in their neighbourhood, or to all other users.
This is done not using the permission system but from a config field on the cforge settings page.
Users can opt out of these 'broadcast' mail notifications.
Each node can only be broadcast once.
