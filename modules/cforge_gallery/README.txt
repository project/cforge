Gallery module creates a content-type, photo, and a vocabulary, galleries for categorising it.
A gallery page leads to individual galleries where each photo can be viewed full size.
A block shows random photos.
