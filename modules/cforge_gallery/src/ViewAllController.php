<?php

namespace Drupal\cforge_gallery;

use Drupal\system\Controller\SystemController;
use Drupal\taxonomy\Entity\Term;

/**
 * Displays the text of the readme file for the given module.
 */
class ViewAllController extends SystemController {

  /**
   * Page callback.
   */
  public function page() {
    $output = [];
    $q = \Drupal::database()->select('taxonomy_index', 'i')->fields('i', ['nid', 'tid']);
    $q->join('taxonomy_term_data', 'td', 'i.tid = td.tid');
    $q->condition('td.vid', 'galleries');
    $galleries = [];
    foreach ($q->execute()->fetchAllKeyed() as $nid => $tid) {
      $galleries[$tid][] = $nid;
    }

    foreach ($galleries as $tid => $nids) {
      $gallery = Term::load($tid);
      $output[] = [
        '#theme' => 'gallery_preview',
        '#taxonomy_term' => $gallery,
        '#nids' => $nids,
      ];
    }
    if (empty($output)) {
      $output['#markup'] = t('There are no photos.');
    }
    return $output;
  }

}
