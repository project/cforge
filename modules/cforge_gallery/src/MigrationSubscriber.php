<?php

namespace Drupal\cforge_gallery;

use Drupal\migrate\MigrateSkipRowException;
use Drupal\migrate\Event\MigratePreRowSaveEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Migration modifications
 *
 * @todo inject \Drupal::entityQuery('taxonomy_term')
 */
class MigrationSubscriber implements EventSubscriberInterface {


  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() : array {
    return [
      'migrate.pre_row_save' => [['migratePreRowSave']]
    ];
  }

  /**
   * @param Drupal\migrate\Event\MigratePreRowSaveEvent $event
   */
  public function migratePreRowSave(MigratePreRowSaveEvent $event) {
    $row = $event->getRow();
    $migration = $event->getMigration();

    if ($migration->id() == 'd7_taxonomy_vocabulary') {
      if ($row->getSourceProperty('machine_name') == 'galleries') {
        throw new MigrateSkipRowException();
      }
    }

    // Copy the term references to the new 'terms' field
    if ($migration->id() == 'd7_node:image') {
      $row->setDestinationProperty('terms', $row->getDestinationProperty('galleries'));
    }

    // Don't copy the categories field or field instance
    if ($migration->id() == 'd7_field' or $migration->id() == 'd7_field_instance') {
      if ($row->getSourceProperty('field_name') == 'galleries') {
        throw new MigrateSkipRowException('Galleries term reference field is already installed.');
      }
      elseif ($row->getSourceProperty('field_name') == 'image') {
        throw new MigrateSkipRowException('Node image field is already installed.');
      }
    }

    // Don't copy the node_type
    if ($migration->id() == 'd7_node_type') {
      if ($row->getSourceProperty('type') == 'image') {
        throw new MigrateSkipRowException("Image node type is already installed");
      }
    }

    if($migration->id() == 'd7_comment_type' or $migration->id() == 'd7_comment_field') {
      if ($row->getSourceProperty('type') == 'image') {
        throw new MigrateSkipRowException('Comments for images already exist');
      }
    }
  }

}
