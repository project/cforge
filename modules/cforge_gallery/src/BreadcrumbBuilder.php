<?php

namespace Drupal\cforge_gallery;

use Drupal\Core\Link;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\taxonomy\Entity\Term;
use Drupal\Core\Cache\CacheableMetadata;

/**
 * Defines a class to build path-based breadcrumbs.
 *
 * @see \Drupal\Core\Breadcrumb\BreadcrumbBuilderInterface
 */
class BreadcrumbBuilder extends \Drupal\cforge\NodeBreadcrumbBuilder {
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match, ?CacheableMetadata $cacheable_metadata = null) {
    if ($route_match->getParameter('node')) {
      return $route_match->getParameter('node')->getType() == 'image';
    }
    elseif (in_array($route_match->getRouteName(), ['view.gallery.page_1', 'cforge.galleries'])) {
      return TRUE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function build(RouteMatchInterface $route_match) {
    $links = [
      Link::createFromRoute($this->t('Galleries'), 'cforge.galleries')
    ];
    if ($route_match->getRouteName() == 'view.gallery.page_1') {
      // The name of the gallery from the views arg
      $tid = $route_match->getParameter('arg_0');
      $links[] = Link::createFromRoute(Term::load($tid)->label(), '<current>');
    }
    elseif ($node = $route_match->getParameter('node')) {
      $links[] = Link::createFromRoute($node->terms->entity->label(), 'view.gallery.page_1', ['arg_0' => $node->terms->target_id]);
      $links = array_merge($links, $this->nodeLinks($route_match));
    }
    return $this->breadcrumbs($links);
  }

}
