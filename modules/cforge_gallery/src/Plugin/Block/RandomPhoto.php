<?php

namespace Drupal\cforge_gallery\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Block\Attribute\Block;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Shows a random photo.
 *
 * @Block(
 *   id = "random_photo",
 *   label = "Random photo",
 *   admin_label = @Translation("Cforge random photo"),
 *   category = "Hamlets"
 * )
 */
#[Block(
  id: 'random_photo',
  label: new TranslatableMarkup('Random photo'),
  admin_label: new TranslatableMarkup('Random photo'),
  category: new TranslatableMarkup('Hamlets')
)]
class RandomPhoto extends BlockBase implements ContainerFactoryPluginInterface {

  private $entityTypeManager;
  private $nid;

  /**
   * Constructor.
   */
  public function __construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $nids = $entity_type_manager->getStorage('node')
      ->getQuery()->accessCheck(TRUE)
      ->condition('type', 'image')
      ->execute();
    if ($nids) {
      $nids= array_values($nids);
      $this->nid = $nids[rand(0, count($nids) - 1)];
    }
  }


  /**
   * Injection.
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static (
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Don't know why the label isn't rendering automatically.
    return [
      '#title' => $this->label()
    ] + cforge_gallery_format_photo($this->nid);
  }

  /**
   * Prevent anon from seeing photos.
   *
   * @param AccountInterface $account
   *   The current user.
   *
   * @return AccessResult
   *   The Access result.
   */
  public function blockAccess(AccountInterface $account) {
    return $account->isAuthenticated() ?
      AccessResult::allowedIf($this->nid) :
      AccessResult::forbidden();
  }

}
