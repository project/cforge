<?php

/**
 * Implements hook_form_system_theme_settings_alter().
 */
function hbk_cforge_form_system_theme_settings_alter(&$form, &$form_state) {
  $settings = \Drupal::Config('hbk_cforge.settings');
  $form['hbk_cforge'] = [
    '#type' => 'details',
    '#title' => t('Select a color'),
    '#tree' => FALSE,
    '#open' => TRUE,
    '#weight' => 0,
    'current_theme' => [
      '#type' => 'select',
      '#title' => t('Theme color'),
      "#options" => [
        "teal" => t("Teal"),
        "passion" => t("Passion"),
        "dark" => t("Dark"),
        "purple" => t("Purple"),
        "orange" => t("orange"),
        "brown" => t('brown'),
        "blue" => t('blue'),
        "green" => t('green')
      ],
      '#default_value' => $settings->get('current_theme')
    ]
  ];
  $form['footer_menu_orientation'] = [
    'footer_menu_orientation' => [
      '#title' => t('Footer menu orientation'),
      '#description' => t('For any menu in the footer region.'),
      '#type' => 'radios',
      '#options' => [
        'horiz' => t('Horizontal'),
        'vert' => t('Vertical')
      ],
      '#default_value' => $settings->get('footer_menu_orientation')
    ]
  ];
  $form["#submit"][] = 'drupal_flush_all_caches';
}

