<?php

namespace Drupal\cforge\Controller;

use Symfony\Component\HttpFoundation\Response;

/**
 * Class controller.
 * Can only be called from a views path. See
 */
class CssController {

  /**
   * Render styles.
   */
  public function dynamicTables() {
    $pos = strpos($_SERVER['HTTP_REFERER'], $_SERVER['HTTP_HOST']) + strlen($_SERVER['HTTP_HOST']);
    $path = substr($_SERVER['HTTP_REFERER'], $pos);

    $request = Symfony\Component\HttpFoundation\Request::create($path);
    $response = \Drupal::service('http_kernel')->handleRaw($request);

    // This
    // from the url, we have to get the view.
    return $this->css([
      $_SERVER['HTTP_REFERER'] => ['hello' => 'goodbye']
    ]);

    return $this->css([
      'h1, h2' => [
        'color' => 'red !important'
      ]
    ]);

    $view_id = 'view-display-id-'.$vars['view']->current_display;
    foreach ($vars['header'] as $field_name => $info) {
      if ($label = $info['content']) {// The translated label
        $class = $vars['fields'][$field_name];
        $css[] = "div.$view_id td.views-field-$class:before { content: \"$label\"; }";
      }
    }
  }


  /**
   * @param $data array
   *
   * @return Response
   */
  protected function css($data) {
    $content = "";
    foreach ($data as $selectors => $styles) {
      $content .= $selectors . '{';
      foreach ($styles as $key => $value) {
        $content .= sprintf('%s: %s;', $key, $value);
      }
      $content .= '}';
    }

    $response = new Response($content);
    $response->headers->set('content-type','text/css');
    return $response;
  }


}


