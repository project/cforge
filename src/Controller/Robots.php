<?php

namespace Drupal\cforge\Controller;

use Drupal\system\Controller\SystemController;
use Symfony\Component\HttpFoundation\Response;

/**
 * Returns a robots.text file.
 */
class Robots extends SystemController {

  /**
   * Page callback.
   *
   * @todo
   * @see https://api.drupal.org/api/drupal/core!includes!bootstrap.inc/function/_drupal_add_http_header/8
   * Call to undefined method Symfony\\Component\\HttpFoundation\\Response::create()
   */
  public function page() {
    // I think the Content-type text/plain header will be set automatically.
    if (\Drupal::Config('cforge.settings')->get('robots_protect')) {
      $content = 'User-agent: *
      Disallow: /';
    }
    else {
      $robotsFile = \Drupal::service('extension.path.resolver')->getPath('module', 'cforge') .'/robots.txt';
      $content = file_get_contents($robotsFile);
    }
    return new Response($content, 200);
  }

}
