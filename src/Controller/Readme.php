<?php

namespace Drupal\cforge\Controller;

use \Drupal\system\Controller\SystemController;

/**
 * Displays the text of the readme file for the given modue.
 */
class Readme extends SystemController {

  /**
   * Page callback.
   */
  public function page($module) {
    $path = \Drupal::service('extension.path.resolver')->getPath('module', $module);
    return array(
      '#markup' => _filter_autop(file_get_contents($path . '/README.txt')),
    );
  }

  /**
   * Title callback.
   */
  public function title($module) {
    return t('@module README file', ['@module' => $module]);
  }

}
