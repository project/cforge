<?php

namespace Drupal\cforge\Controller;

use Drupal\system\Controller\SystemController;
use Drupal\user\Entity\User;

/**
 * Returns a robots.text file.
 */
class General extends SystemController {

  /**
   * Page callback.
   * This was being passed a user object until hbk theme started trying to setTitle
   */
  public function userTitle($user) {
    if (!is_object($user)) {
      $user = \Drupal\user\Entity\User::load($user);
    }
    return $user->getDisplayName();
  }

  /**
   * Form title callback.
   *
   * @param User $user
   * @return Drupal\Core\Render\Markup
   */
  public function userAdminPageTitle(User $user) {
    return $this->t('Administer @username', ['@username' => $user->getDisplayName()]);
  }
}
