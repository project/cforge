<?php

/**
 * @file
 * Contains \Drupal\cforge\Controller\StepByStep.
 */
namespace Drupal\cforge\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Routing\TrustedRedirectResponse;


class StepByStep extends ControllerBase {

  function externalRedirect() {
    if ($this->languageManager()->getCurrentLanguage()->getId() == 'fr') {
      $url = 'http://helpdesk.communityforge.net/fr/step-by-step';
    }
    else {
      $url = 'http://helpdesk.communityforge.net/en/step-by-step';
    }
    return new TrustedRedirectResponse($url);

  }

}

