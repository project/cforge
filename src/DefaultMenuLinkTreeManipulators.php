<?php

namespace Drupal\cforge;

use Drupal\Core\Menu\MenuLinkInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Control access to the nodes withthe publiconly flag.
 */
class DefaultMenuLinkTreeManipulators extends \Drupal\Core\Menu\DefaultMenuLinkTreeManipulators {

  /**
   * Checks access for one menu link instance.
   *
   * @param \Drupal\Core\Menu\MenuLinkInterface $instance
   *   The menu link instance.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  protected function menuLinkCheckAccess(MenuLinkInterface $instance) {
    $access_result = parent::menuLinkCheckAccess($instance);
    if (in_array($instance->getMenuName(), ['main', 'secondary'])) {
      $url = $instance->getUrlObject();
      if ($url->isRouted() and $params = $url->getRouteParameters()) {
        if (isset($params['node'])) {
          $result = AccessResult::allowedIf(cforge_node_get_publiconly($params['node']) == $this->account->isAnonymous());
          $access_result->andif($access_result, $result);
        }
      }
    }
    return $access_result;
  }

}
