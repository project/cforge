<?php

namespace Drupal\cforge;

use Drupal\aggregator\Entity\Feed;
use Drupal\filter\Entity\FilterFormat;

/**
 * Defines a chained translation implementation combining multiple translators.
 */
class Secure implements \Drupal\Core\Security\TrustedCallbackInterface {

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return ['textFilterMap', 'aggregatorExternalLinks', 'dateAfterNow'];
  }

  /**
   * Element pre_render callback.
   *
   * Map text formats onto existing formats.
   */
  static function textFilterMap($element) {
    static $map;
    if (!isset($map)) {
      $formats = array_keys(FilterFormat::loadMultiple());
      $map = array_combine($formats, $formats);
      $map['basic'] = 'basic_html';
      $map['restricted_html'] = 'basic_html';
    }
    $element['#format'] = $map[$element['#format']]?? 'basic_html';
    return $element;
  }


  static function aggregatorExternalLinks($build) {
    if (isset($build['#configuration'])) {
      if ($feed = Feed::load($build['#configuration']['feed'])) {
        $build['#configuration']['label'] = $feed->label();
      }
      else {
        trigger_error('Unable to load feed '.$build['#configuration']['feed'], E_USER_WARNING);
      }
      foreach ($build['content']['list']['#items'] as $item) {
        $item['#url']->setOption('attributes', ['target' => '_blank']);
      }
    }
    return $build;
  }

  static function dateAfterNow(&$element) {
    unset($element['year']['#empty_option']);
    $element['year']['#default_value'] = '2023';
  }

}
