<?php

namespace Drupal\cforge\Forms;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form builder for Cforge settings.
 */
class Settings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cforge_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildform($form, $form_state);
    $config = $this->config('cforge.settings');

    $committee = \Drupal::entityTypeManager()
      ->getStorage('user')
      ->loadByProperties(['roles' => 'committee']);
    $form['account_manager'] = [
      '#title' => $this->t('Account manager'),
      '#description' => $committee ? $this->t('The committee member who handles new members.') : 'No committee members',
      '#type' => 'select',
      '#options' => array_map(function ($c) {return $c->getDisplayName();}, $committee),
      '#default_value' => $config->get('account_manager'),
      '#required' => FALSE,
      '#weight' => 1
    ];

    $form['robots_protect'] = [
      '#title' => $this->t('Conceal from search engines'),
      '#description' => $this->t('Rewrites the robots.txt file'),
      '#type' => 'checkbox',
      '#default_value' => $config->get('robots_protect', 0),
      '#weight' => 5,
    ];
    $form['mail_footer'] = [
      '#title' => $this->t('Footer'),
      '#description' => $this->t('Appended to all mails sent by the site to members. HTML is allowed.'),
      '#type' => 'textarea',
      '#rows' => 4,
      '#default_value' => $config->get('mail_footer'),
      '#weight' => 6,
    ];

    $saved = $config->get('social_links');
    $socials = [
      'Facebook' => 'https://facebook.com/YOURNAME',
      'X' => 'https://x.com/YOURNAME',
      'Youtube' => 'https://youtube.com/YOURNAME',
      'Linkedin' => 'https://linkedin.com/YOURNAME',
      'Github' => 'https://github.com/YOURNAME',
      'Instagram' => 'https://instagram.com/YOURNAME',
      'SnapChat' => 'https://snapchat.com/YOURNAME',
      'Pinterest' => 'https://pinterest.com/YOURNAME'
    ];
    $form['social_links'] = [
      '#type' => 'details',
      '#title' => t('Links to social networks'),
      '#description' => t('If set, links appear towards the bottom of the page. (HBK theme only)'),
      '#tree' => TRUE
    ];

    foreach ($socials as $label => $url_template) {
      $key = strtolower($label);
      $form['social_links'][$key] = [
        '#type' => 'url',
        '#title' => $this->t("Your @network account", ['@network' => $label]),
        '#default_value' => $saved[$key] ?? "",
        '#placeholder' => $url_template
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $config = $this->configFactory->getEditable('cforge.settings');
    $config
      ->set('mail_footer', $values['mail_footer'])
      ->set('robots_protect', $values['robots_protect'])
      ->set('account_manager', $form_state->getValue('account_manager'))
      ->set('social_links', array_filter($form_state->getValue('social_links')))
      ->save();
    \Drupal\Core\Cache\Cache::invalidateTags(['*']);
  }

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames() {
    return ['cforge.settings'];
  }

}
