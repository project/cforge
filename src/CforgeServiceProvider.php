<?php

namespace Drupal\cforge;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Modifies Drupal's services
 */
class CforgeServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    $container->getDefinition('menu.default_tree_manipulators')
     ->setClass('\Drupal\cforge\DefaultMenuLinkTreeManipulators');

    $container->getDefinition('path.matcher')
      ->setClass('\Drupal\cforge\PathMatcher')
      ->addArgument(new Reference('current_user'));
  }

}
