<?php

namespace Drupal\cforge\EventSubscriber;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Cforge react to events.
 */
class CforgeSubscriber implements EventSubscriberInterface {

  private $currentUser;
  private $cforgeSettings;

  function __construct($current_user, $config) {
    $this->currentUser = $current_user;
    $this->cforgeSettings = $config->get('cforge.settings');
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() : array {
    $events = [
      KernelEvents::REQUEST  => [['cforgeBlockBot', -1001]],
      KernelEvents::RESPONSE  => [['cforgeFront', 1001]]
    ];
    return $events;
  }

  /**
   * Redirect users
   */
  public function cforgeFront(ResponseEvent $event) {
    // Don't run this when site is in maintenance mode.
    if (\Drupal::state()->get('system.maintenance_mode')) {
      return;
    }
    // Ignore non index.php requests (like cron).
    if (!empty($_SERVER['SCRIPT_FILENAME']) && realpath(DRUPAL_ROOT . '/index.php') != realpath($_SERVER['SCRIPT_FILENAME'])) {
      return;
    }
    if ($this->currentUser->isAuthenticated()) {
      $path = trim($event->getRequest()->getpathinfo(), '/');
      if (empty($path)) {
        $front = $this->cforgeSettings->get('member_frontpage');
        $event->setResponse(new RedirectResponse($front));
      }
    }
  }

  public function cforgeBlockBot(RequestEvent $event) {
    // Ignore non index.php requests (like cron).
    if (!empty($_SERVER['SCRIPT_FILENAME']) && realpath(DRUPAL_ROOT . '/index.php') != realpath($_SERVER['SCRIPT_FILENAME'])) {
      return;
    }
    //@todo
//    if ($this->currentUser->isAnonymous()) {
//      // This was originally added to deter certain kinds of bots probing long paths
//      $path = trim($event->getRequest()->getpathinfo(), '/');
//      $parts = array_filter(explode('/', $path));
//      if (count($parts) > 5) {
//        $response = new Response(NULL, 404);
//        $event->setResponse($response);
//        $this->logger->notice("Blocked access to path %path", ['%path' => $path]);
//      }
//    }
  }


}
