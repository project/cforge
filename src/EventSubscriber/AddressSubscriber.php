<?php

namespace Drupal\cforge\EventSubscriber;

use \Drupal\address\Event\AddressFormatEvent;
use \Drupal\address\Event\AddressEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use CommerceGuys\Addressing\AddressFormat\DependentLocalityType;

/**
 * Tweaks the address formats.
 */
class AddressSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritDoc}
   */
  public static function getSubscribedEvents() : array {
    $events = [];
    // Otherwise this fails during installation.
    if (class_exists('\Drupal\address\Event\AddressEvents')) {
      $events[AddressEvents::ADDRESS_FORMAT] = [['rewriteAddress', -10]];
    }
    return $events;
  }

  /**
   * Ensure every address format has %dependentLocality, sticking it on the end
   * if necessary, that that field is required, and called neighbourhood
   */
  public function rewriteAddress(AddressFormatEvent $event) {
    $address_format = $event->getDefinition();
    $address_format['dependent_locality_type'] = DependentLocalityType::NEIGHBORHOOD;
    $address_format['required_fields'] = [];
    $address_format['format'] = str_replace(["%givenName", "%familyName"], '', $address_format['format']);

    // If the alt_login module uses the firstname/lastname, put them at the top.
    if (\Drupal::moduleHandler()->moduleExists('alt_login')) {
      $settings = \Drupal::config('alt_login.settings');
      $str = implode($settings->get('aliases')).$settings->get('display');
      if (str_contains($str, 'address')) {
        $address_format['format'] = "%givenName %familyName\n".trim($address_format['format']);
        $address_format['required_fields'] = ['givenName'];
      }
    }
    $event->setDefinition($address_format);
  }

}

