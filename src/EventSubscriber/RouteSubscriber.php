<?php

namespace Drupal\cforge\EventSubscriber;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;
use Drupal\Core\Routing\RoutingEvents;

/**
 * Alter routes.
 *
 * Change some routes into admin routes. Change some route titles.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritDoc}
   *
   * Anything the committee does shouldn't be an admin_route.
   */
  protected function alterRoutes(RouteCollection $collection) {
    $route_names = [
      'entity.taxonomy_term.edit_form',
      'entity.taxonomy_term.delete_form',
      'entity.user.edit_form',
    ];
    foreach ($collection as $route_name => &$route) {
      if (in_array($route_name, $route_names)) {
        $route->setOption('_admin_route', FALSE);
      }
    }
    $collection->get('system.admin_content')
      ->setRequirements(['_permission' => 'access content overview']);

    // Hamlets theme does not permit the adding of new menus.
    if ($route = $collection->get('entity.menu.add_form')) {
      $route->setRequirement('_permission', '_none_');
    }

    // Setup route only appears if no users have been created.
    $uids = \Drupal::entityTypeManager()->getStorage('user')->getQuery()->accessCheck(FALSE)->execute();
    if (count($uids) < 3) {
      $route = (new \Symfony\Component\Routing\Route('setup'))
        ->setDefaults(['_form' => 'Drupal\cforge\Setup', '_title' => 'Installation options'])
        ->setRequirement('_permission', 'access administration pages');
      $collection->add('cforge.setup', $route);
    }
    // Make all page titles under user/ the same
    // If not overridden, this works for non-views pages
    foreach ($collection->all() as $rid => $route) {
      if (substr($route->getPath(), 0, 13) == '/user/{user}/') {
        $route->setDefault('_title_callback', '\Drupal\cforge\Controller\General::userTitle');
      }
    }

    // Prevent local admin editing themes other than the current theme.
    $collection->get('system.theme_settings_theme')
      ->setRequirements(['_is_active_theme' => 'TRUE', '_role' => 'local_admin']);

  }

}
