<?php

namespace Drupal\cforge;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountProxy;
use Drupal\Core\Url;

/**
 * Provides a path matcher, with a different front page for authenticated and
 * anonymous users
 */
class PathMatcher extends \Drupal\Core\Path\PathMatcher {

  /**
   * TRUE if the current user is authenticated
   * @var bool
   */
  private $authenticated;

  /**
   * Creates a new PathMatcher.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current route match.
   */
  public function __construct(ConfigFactoryInterface $config_factory, RouteMatchInterface $route_match, AccountProxy $current_user) {
    $this->configFactory = $config_factory;
    $this->routeMatch = $route_match;
    $this->authenticated = $current_user->isAuthenticated();
  }

  /**
   * {@inheritdoc}
   */
  public function isFrontPage() {
    if (!isset($this->isCurrentFrontPage)) {
      if ($this->authenticated) {
        $this->isCurrentFrontPage = FALSE;
        // Ensure that the code can also be executed when there is no active
        // route match, like on exception responses.
        if ($this->routeMatch->getRouteName()) {
          $url = Url::fromRouteMatch($this->routeMatch);
          $this->isCurrentFrontPage = ($url->getRouteName() && '/' . $url->getInternalPath() === $this->authFront());
        }
      }
      else {
        parent::isFrontPage();
      }
    }
    return $this->isCurrentFrontPage;
  }

  /**
   * Gets the current front page path.
   *
   * @return string
   *   The front page path.
   */
  protected function getFrontPagePath() {
    if (!isset($this->isCurrentFrontPage) and $this->authenticated) {
      $this->frontPage = $this->authFront();
    }
    elseif (!isset($this->frontPage)) {
      $this->frontPage = parent::getFrontPagePath();
    }
    return $this->frontPage;
  }

  /**
   * Get the pathname of the cforge front page.
   * @return string
   */
  private function authFront() {
    return $this->configFactory->get('cforge.settings')
      ->get('member_frontpage');
  }

}
