<?php

namespace Drupal\cforge;

use Drupal\Core\Breadcrumb\BreadcrumbBuilderInterface;
use Drupal\Core\Breadcrumb\Breadcrumb;
use Drupal\Core\Link;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Cache\CacheableMetadata;

/**
 * A class to build breadcrumbs for all node routes.
 * It doesn't do views. Note that the 'news' view is the default home page.
 */
class NodeBreadcrumbBuilder implements BreadcrumbBuilderInterface {
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match, ?CacheableMetadata $cacheable_metadata = null) {
    switch ($route_match->getRouteName()) {
      case 'entity.node.edit_form':
      case 'entity.node.canonical':
      case 'node.add':
      case 'node.add_page':
        return TRUE;
    }
  }
  /**
   * {@inheritdoc}
   */
  public function build(RouteMatchInterface $route_match) {
    if ($route_match->getRouteName() == 'node.add_page') {
      $links[] = Link::createFromRoute($this->t('Create'), '<current>');
    }
    elseif ($route_match->getRouteName() == 'node.add') {
      $bundle = $route_match->getParameter('node_type');
      $links[] = Link::createFromRoute($this->t('Create'), '<current>');
      $links[] = Link::createFromRoute($bundle->label(), 'node.add', ['node_type' => $bundle->id()]);
    }
    else {
      $links = $this->nodeLinks($route_match);
    }
    return $this->breadcrumbs($links)->addCacheTags(['node:'.$route_match->getRawparameter('node')]);
  }

  /**
   * Create the last links for node view/edit page.
   *
   * @param RouteMatchInterface $route_match
   */
  protected function nodeLinks(RouteMatchInterface $route_match) : array {
    // Final link is the name of the node
    $node = $route_match->getParameter('node');
    if ($route_match->getRouteName() == 'entity.node.edit_form') {
      $links[] = Link::createFromRoute($node->label(), 'entity.node.canonical', ['node' => $node->id()]);
      $links[] = Link::createFromRoute($this->t('Edit'), '<current>');
    }
    elseif ($route_match->getRouteName() == 'entity.node.canonical') {
      $links[] = Link::createFromRoute($node->label(), '<current>');
    }
    return $links;
  }

  /**
   * Prefix the home link and convert the links to breadcrumbs.
   *
   * @param array $links
   * @return type
   */
  protected function breadcrumbs(array $links) : Breadcrumb {
    array_unshift($links, Link::createFromRoute($this->t('Home'), '<front>'));
    return (new Breadcrumb())
      ->setLinks($links)
      ->addCacheContexts(['url.path.parent', 'url.path.is_front']);
  }



}
