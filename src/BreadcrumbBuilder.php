<?php

namespace Drupal\cforge;

use Drupal\system\PathBasedBreadcrumbBuilder;
use Drupal\Core\Breadcrumb\Breadcrumb;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Routing\RouteMatch;
use Drupal\Core\Cache\CacheableMetadata;

/**
 * Defines a class to build path-based breadcrumbs.
 *
 * @see \Drupal\Core\Breadcrumb\BreadcrumbBuilderInterface
 */
class BreadcrumbBuilder extends PathBasedBreadcrumbBuilder {

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match, ?CacheableMetadata $cacheable_metadata = null) {
    return !$route_match->getRouteObject()->getOption('_admin_route');
  }

  /**
   * {@inheritdoc}
   */
  public function build(RouteMatchInterface $route_match) {
    $breadcrumb = new Breadcrumb();
    if ($this->pathMatcher->isFrontPage()) {
      return $breadcrumb;
    }
    $links = [];
    // Always start with Home.
    $links[] = Link::createFromRoute($this->t('Home'), '<front>');
    [$p1, $p2, $p3] = array_pad(explode('.', $route_match->getRouteName()), 3, 0);
    if ($p1 == 'view') {
      if (in_array($p2, ['user_admin_people', 'members', 'membermap'])) {
        $links[] = new Link($this->t('Members'), Url::fromRoute('view.members.member_page'));
      }
      else {
        $links[] = new Link($this->getViewTitle($p2, $p3), Url::fromRoute('<current>'));
      }
    }
    elseif ($p1 == 'entity' and $p2 == 'user' or $route_match->getRouteName() == 'gdpr.collected_user_data') {
      $user = $route_match->getParameter('user');
      $links[] = Link::createFromRoute($user->getDisplayName(), 'entity.user.canonical', ['user' => $user->id()]);
      if ($p2 == 'user' and $p3 <> 'canonical') {
        $links[] = new Link(t('Edit'), Url::fromRoute('entity.user.canonical', ['user' => $user->id()]));
      }
      elseif ($p1 == 'gdpr') {
        $links[] = new Link(t('Your Data'), Url::fromRoute('<current>'));
      }
      switch ($p3) {
        case 'profile':
          $links[] = new Link(t('Profile'), Url::fromRoute('<current>'));
          break;
        case 'edit_form':
          $links[] = new Link(t('Account'), Url::fromRoute('<current>'));
          break;
        case 'admin':
          $links[] = new Link(t('Admin'), Url::fromRoute('<current>'));
          break;
      }
    }
    return $breadcrumb->setLinks($links)->addCacheContexts(['url.path.parent', 'url.path.is_front']);
  }


  private function getViewTitle($view_id, $display_id) :string {
    $view = \Drupal\views\Views::getView($view_id);
    $view->setDisplay($display_id);
    $title = $view->getDisplay()->getOption('title');
    if (!$title) {
      $view->setDisplay('default');
      $title = $view->getDisplay()->getOption('title');
      if (!$title) {
        \Drupal::logger('breadcrumbs')->warning('No title for view %view display %display', ['%view' => $view_id, '%display' => $display_id]);
        $title = 'Untitled!!';
      }
    }
    return $title;
  }

  private function getLinkFromRouteMatch($route_match) : Link {
    $path = $route_match->getRouteObject()->getPath();
    if ($route_request = $this->getRequestForPath($path, [])) {
      $temp_route_match = RouteMatch::createFromRequest($route_request);
      $title = $this->titleResolver->getTitle($route_request, $temp_route_match->getRouteObject());
    }
    return new Link($title, Url::fromRoute('<current>'));
  }

}
