<?php

namespace Drupal\cforge\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Drupal\views\Attribute\ViewsField;

/**
 * @ingroup views_field_handlers
 */
#[ViewsField('cf_contactme')]
class ContactMe extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $raw = $this->getValue($values);
    return ['#markup' => 'The cf_contactme plugin is unfinished'];
  }

}
