<?php

namespace Drupal\cforge\Plugin\views\display;

use Drupal\views\Plugin\views\display\ResponseDisplayPluginInterface;
use Drupal\views\Plugin\views\display\PathPluginBase;
use Drupal\views\ViewExecutable;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\CacheableResponse;
use Drupal\views\Attribute\ViewsDisplay;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Renders the view plainly as a full page view.
 *
 * This overrides the REST Export display to make labelling clearer on the admin
 * UI, and to allow attaching of these to other displays.
 *
 * @ingroup views_display_plugins
 */
#[ViewsDisplay(
  id: 'cforge_print_page',
  title: new TranslatableMarkup(''),
  help: new TranslatableMarkup(''),
  uses_route: TRUE,
  admin: new TranslatableMarkup(''),
  returns_response: TRUE,
  theme: 'views-view'
)]
class PrintPage extends PathPluginBase implements ResponseDisplayPluginInterface {

  public static function buildResponse($view_id, $display_id, array $args = []) {
    $build = static::buildBasicRenderable($view_id, $display_id, $args);
    // Set up an empty response, so for example RSS can set the proper
    // Content-Type header.
    $response = new CacheableResponse('', 200);
    $build['#response'] = $response;
    $output = (string) \Drupal::service('renderer')->renderRoot($build);
    if (empty($output)) {
      $output = 'Nothing to print!';
    }

    $response->setContent($output);
    $cache_metadata = CacheableMetadata::createFromRenderArray($build);
    $response->addCacheableDependency($cache_metadata);
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function execute() {
    parent::execute();
    return $this->view->render();
  }

  /**
   * {@inheritdoc}
   */
  public function attachTo(ViewExecutable $clone, $display_id, array &$build) {
    $displays = $this->getOption('displays');
    die('attachTo');
    if (empty($displays[$display_id])) {
      return;
    }

    if (!$this->access()) {
      return;
    }

    // Defer to the feed style; it may put in meta information, and/or
    // attach a feed icon.
    $clone->setArguments($this->view->args);
    $clone->setDisplay($this->display['id']);
    $clone->buildTitle();
    $displays = $clone->storage->get('display');
    $title = $clone->getTitle();

    if (!empty($displays[$this->display['id']])) {
      $title = $displays[$this->display['id']]['display_title'];
    }

    if ($plugin = $clone->display_handler->getPlugin('style')) {
      $plugin->attachTo($build, $display_id, $clone->getUrl(), $title);
      foreach ($clone->feedIcons as $feed_icon) {
        $this->view->feedIcons[] = $feed_icon;
      }
    }

    // Clean up.
    $clone->destroy();
    unset($clone);
  }

}
