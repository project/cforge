<?php

namespace Drupal\cforge\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Block\Attribute\Block;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Provides welcome video for new members of the site.
 */
#[Block(
  id: 'hamlets_vid',
  admin_label: new TranslatableMarkup('Hamlets intro vid'),
  category: new TranslatableMarkup('Hamlets')
)]
class HamletsVid extends BlockBase implements ContainerFactoryPluginInterface {

  private $languageManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('language_manager')
    );
  }

  /**
   * Construction.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, $language_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    switch ($this->languageManager->getCurrentLanguage()->getId()) {
      case 'fr':
        $label = 'Video découvrir le site';
        $url = 'https://www.youtube.com/embed/En1RWomDFgI';
        break;

      case 'es':
        $label = 'Darse de alta en la Central de Talentos';
        $url = 'https://www.youtube.com/embed/pEdk1gct0Iw?list=PL2C2A689416705667';
        break;

      case 'en':
        $label = 'Mutual Credit';
        $url = 'https://www.youtube.com/embed/YvegNqKcQ-g';
        break;

      default:
        return [];
    }
    $this->setConfiguration(['label' => $label]);
    $html = '<iframe width = "100%" height = "184" src = "'.$url.'" frameborder = "0" allowfullscreen></iframe>';
    return [
      '#label' => $label,
      '#markup' => Markup::create($html),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['label']['#access'] = FALSE;
    return $form;
  }

}
