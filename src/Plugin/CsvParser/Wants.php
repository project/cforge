<?php

namespace Drupal\cforge\Plugin\CsvParser;
use Drupal\cforge_import\Plugin\CsvParser\Smallads;

/**
 * Plugin to import wants.
 *
 * @Plugin(
 *   id = "wants",
 *   label = "Wants",
 *   entity_type = "smallad",
 *   bundle = "want"
 * )
 */
class Wants extends Smallads {

  const BUNDLE = 'want';

}
