<?php

namespace Drupal\cforge\Plugin\CsvParser;
use Drupal\cforge_import\Plugin\CsvParser\Smallads;

/**
 * Plugin to import offers.
 *
 * @Plugin(
 *   id = "offers",
 *   label = "Offers",
 *   entity_type = "smallad",
 *   bundle = "offer"
 * )
 */
class Offers extends Smallads {

  const BUNDLE = 'offer';

  /**
   * {@inheritdoc}
   */
  public function columns() {
    $columns = parent::columns();
    return $columns + [
      'categories' => t('semicolon separated term names'),
      'picture' => t('file path for downloading'),
    ];
  }

}
