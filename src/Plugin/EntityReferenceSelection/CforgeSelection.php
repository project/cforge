<?php

namespace Drupal\cforge\Plugin\EntityReferenceSelection;

use Drupal\Core\Entity\Attribute\EntityReferenceSelection;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Form\FormStateInterface;
use Drupal\alt_login\Plugin\EntityReferenceSelection\AltLoginUserSelection;

/**
 * Provides specific access control for the user entity type.
 */
#[EntityReferenceSelection(
  id: "default:cforge",
  label: new TranslatableMarkup("Trader selection"),
  entity_types: ["user"],
  group: "cforge",
  weight: 1
)]
class CforgeSelection extends AltLoginUserSelection {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function buildEntityQuery($match = NULL, $match_operator = 'CONTAINS') {
    $query = parent::buildEntityQuery($match, $match_operator);
    $cur_user = \Drupal::currentUser();
    $user_has_roles = $cur_user->getRoles(TRUE);
    if (!in_array('local admin', $user_has_roles) and $cur_user->id() <> 1) {
      $excluded_roles[] = 'local admin';
    }
    if (!in_array(RID_COMMITTEE, $user_has_roles) and $cur_user->id() <> 1) {
      $excluded_roles[] = RID_COMMITTEE;
    }
    if (isset($excluded_roles)) {
      $excluded_uids = $this->entityTypeManager->getStorage('user')
        ->getQuery()->accessCheck(TRUE)
        ->condition('roles', $excluded_roles, 'IN')
        ->execute();
      if ($excluded_uids) {
        $query->condition('uid', $excluded_uids, 'NOT IN');
      }
    }
    return $query;
  }

}
