<?php

namespace Drupal\cforge\Plugin\Field\FieldFormatter;

use Drupal\cforge\Plugin\Field\FieldType\ContactMeItem;
use Drupal\user\UserData;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Session\AccountProxy;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Field\Attribute\FieldFormatter;
use Drupal\Core\Render\Element;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Formatter plugin implementation foro the 'Contact me' fieldtype.
 */
#[FieldFormatter(
  id: 'cf_contactme',
  label: new TranslatableMarkup('Contact me channels'),
  description: new TranslatableMarkup('Phone numbers and social media channels'),
  field_types: ['cf_contactme']
)]
class ContactMeFormatter extends FormatterBase  {

  /**
   * @var AccountProxy
   */
  private $currentUser;

  /**
   * @var UserData;
   */
  private $userData;

  const PRIVATE_DATA = 'xxxxxxxxxx';

  /**
   *
   * @param type $plugin_id
   * @param type $plugin_definition
   * @param type $field_definition
   * @param type $settings
   * @param type $label
   * @param type $view_mode
   * @param type $third_party_settings
   * @param UserData $user_data
   * @param AccountProxy $current_user
   */
  function __construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings, UserData $user_data, AccountProxy $current_user) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->currentUser = $current_user;
    $this->userData = $user_data;
  }

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static (
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('user.data'),
      $container->get('current_user')
    );
  }

   /**
   * {@inheritDoc}
   */
  public static function defaultSettings() {
    return [
      'channels' => [],
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    if ($this->currentUser->isAuthenticated()) {// @todo ensure field permissions have been checked.
      // Put the definitive email if settings permit.
      if (!empty($this->userData->get('cforge', $items->getEntity()->id(), 'show_email')) or $this->currentUser->hasPermission('view user email addresses')) {
        $email = $items->getEntity()->getEmail();
      }
      else {
        $email = '<em>['. t('Email is private') .']</em>';
      }
      $elements[0] = [
        '#markup' => "<div class = \"channels email\">".$this->t('Email (definitive)').': '.$email.'</div>',
        '#weight' => 0
      ];

      // get the channels permitted by the field settings.
      $available_channels = array_filter($items->getFieldDefinition()->getSetting('channels'));

      $i = 1;
      foreach ($available_channels as $channel) {
        // Todo we should only show the properties enabled in widget settings
        if ($val = $items->{$channel}) {
          $title = ContactMeItem::allChannels()[$channel]['title'];
          $elements[$i++] = [
            '#markup' => '<div class = "channels '.$channel.'" title = "'.$title.'"> '.$items->{$channel.'_formatted'}.'</div>',
            '#weight' => $i
          ];
        }
      }
      if (count(Element::Children($elements))> 2) {
        $elements = [
          '#title' => t('Contact details'),
          '#type' => 'details',
          '#open' => FALSE,
          '#attached' => ['library'=> ['cforge/social_media_icons']],
        ] + $elements;
      }
    }
    return $elements;
  }

  /**
   * {@inheritDoc}
   * Yowch this makes a mess if it should happen more than once.
   */
  function prepareView(array $entities_items) {
    parent::prepareView($entities_items);
    // Requires a lot of scaffolding code to inject into formatters.
    $admin = $this->currentUser->hasPermission('administer users');
    $all_channels = ContactMeItem::allChannels();
    foreach ($entities_items as $items) {
      $show_phone = !empty($this->userData->get('cforge', $items->getEntity()->id(), 'show_phones')) or $admin;
      if ($items->altmail) {
        $url = $all_channels['altmail']['prefix'] . $items->altmail;
        $items->altmail_formatted = '<a href="'.$url.'">'.$items->altmail.'</a>';
      }
      if ($items->whatsapp) { //  Must come before mob and tel
        if ($show_phone) {
          $url = $all_channels['whatsapp']['prefix'] . preg_replace('/^[0-9 ]/', '', $items->{$items->whatsapp});
          $items->whatsapp_formatted = '<a href="'.$url.'">'.$items->{$items->whatsapp}.'</a>';
        }
        else {
          $items->whatsapp_formatted = SELF::PRIVATE_DATA;
        }
      }
      if ($items->tel) {
        if ($show_phone) {
          $url = $all_channels['tel']['prefix'] . preg_replace('/^[0-9 ]/', '', $items->tel);
          $items->tel_formatted = '<a href="'.$url.'">'.$items->tel.'</a>';
        }
        else {
          $items->tel_formatted = SELF::PRIVATE_DATA;
        }
      }
      if ($items->mob) {
        if ($show_phone) {
          $url = $all_channels['mob']['prefix'] . preg_replace('/^[0-9 ]/', '', $items->mob);
          $items->mob_formatted = '<a href="'.$url.'">'.$items->mob.'</a>';
        }
        else {
          $items->mob_formatted = SELF::PRIVATE_DATA;
        }
      }
      if ($items->web) {
        $url = $all_channels['web']['prefix'] . $items->web;
        $items->web_formatted = '<a href="'.$url.'">'.$items->web.'</a>';
      }
      if ($items->twitter) {
        $url = $all_channels['twitter']['prefix'] . $items->twitter;
        $items->twitter_formatted = '<a href="'.$url.'">'.$items->twitter.'</a>';
      }
      if ($items->telegram) {
        $url = $all_channels['telegram']['prefix'] . $items->telegram;
        $items->telegram_formatted = '<a href="'.$url.'">'.$items->telegram.'</a>';
      }
      if ($items->instagram) {
        $url = $all_channels['instagram']['prefix'] . $items->instagram;
        $items->instagram_formatted = '<a href="'.$url.'">'.$items->instagram.'</a>';
      }
      if ($items->linkedin) {
        $url = $all_channels['linkedin']['prefix'] . $items->linkedin;
        $items->linkedin_formatted = '<a href="'.$url.'">'.$items->linkedin.'</a>';
      }
      if ($items->skype) {
        $url = $all_channels['skype']['prefix'] . $items->skype . '?chat';
        $items->skype_formatted = '<a href="'.$url.'">'.$items->skype.'</a>';
      }
      if ($items->facebook) {
        $url = $all_channels['facebook']['prefix'] . $items->facebook;
        $items->facebook_formatted = '<a href="'.$url.'">'.$items->facebook.'</a>';
      }
    }
  }


}
