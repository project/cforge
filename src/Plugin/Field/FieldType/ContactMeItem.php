<?php

namespace Drupal\cforge\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Field\Attribute\FieldType;

/**
 * Plugin implementation of the 'ContactMe' field type.
 */
#[FieldType(
  id: 'cf_contactme',
  label: new TranslatableMarkup('Contact me'),
  description: new TranslatableMarkup('Phone numbers and social media channels'),
  default_widget: 'cf_contactme',
  default_formatter: 'cf_contactme'
)]
class ContactMeItem extends FieldItemBase {

  public static function allChannels() {
    return [
      'altmail' => [
        'title' => t('Email (alternative)'),
        'regex' => '^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$',
        'example' => 'john.doe@example.com',
        'prefix' => 'mailto:'
      ],
      'mob' => [
        'title' => t('Main phone (mobile)'),
        'example' => '99 999 9999',
        'prefix' => 'tel:'
      ],
      'tel' => [
        'title' => t('Second phone'),
        'example' => '99 999 9999',
        'prefix' => 'tel:'
      ],
      'web' => [
        'title' => t('Web site'),
        'prefix' => 'https://',
        'regex' => '^((http|https):\/\/)?[A-Za-z0-9.-]+\.[A-Za-z]{2,6}(?:\/[^\s\/]*)?$',
        'regex' => '^((http|https):\/\/)?[A-Za-z0-9.-]+\.[A-Za-z]{2,6}(?:\/[^\s\/]*)*$',
        'example' => 'example.com'
      ],
      'whatsapp' => [
        'title' => t('Whatsapp number'),
        'prefix' => 'https://wa.me/',
        'phoneid' => TRUE, // not used ATM
        'example' => 'johndoe999'
      ],
      'telegram' => [
        'title' => t('Telegram handle'),
        'prefix' => 'https://telegram.me/',
        'example' => 'johndoe01',
      ],
      'twitter' => [
        'title' => t('Twitter (X) handle'),
        'prefix' => 'https://twitter.com/@',
        'regex' => '^[A-Za-z0-9_]{4,50}$',
        'example' => 'JohnDoe_01'
      ],
      'instagram' => [
        'title' => t('Instagram handle'),
        'prefix' => 'https://instagram.com/',
        'regex' => '^[A-Za-z0-9_.]{4,30}$',
        'example' => 'john.doe_01'
      ],
      'linkedin' => [
        'title' => t('Linkedin handle'),
        'prefix' => 'https://linkedin.com/in/',
        'regex' => '^[A-Za-z0-9_-]{5,}$',
        'example' => 'john-doe_01'
      ],
      'facebook' => [
        'title' => t('Facebook page'),
        'prefix' => 'https://facebook.com/',
        'regex' => '^[a-zA-Z0-9._]{5,}$',
        'example' => 'john.doe_01'
      ],
      'skype' => [
        'title' => t('Skype handle'),
        'prefix' => 'skype:',
        'regex' => '^[a-zA-Z][a-zA-Z0-9\.,\-_]{5,31}$',
        'example' => 'john-doe_01'
      ],
      // can't add this until I know how to update the db.
  //    'discord' => [
  //      'title' => 'Discord handle',
  //      'prefix' => 'discord:',
  //      'regex' => '^[a-z]+:[0-9]{,5}',
  //      'example' => 'johndoe#1234'
  //    ]
    ];
  }

  /**
   * {@inheritDoc}
   */
  public static function propertydefinitions(FieldStorageDefinitionInterface $field_definition) {
    foreach (static::allChannels() as $id => $info) {
      $label = $info['title'];
      $properties[$id] = DataDefinition::create('string')->setLabel($label);
    }
    return $properties;
  }

  /**
   * {@inheritDoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    foreach (static::allChannels() as $id => $info) {
      $columns[$id] = [
        'description' => $info['title'],
        'type' => 'varchar',
        'length' => '32',
      ];
    }
    $columns['web']['length'] = 64;
    $columns['facebook']['length'] = 64;

    return ['columns'=> $columns];
  }

  /**
   * {@inheritDoc}
   */
  public function isEmpty() {
    return ($this->tel && trim($this->tel) and $this->mob && trim($this->mob));
  }

  /**
   * {@inheritDoc}
   */
  public static function defaultFieldSettings() {
    return ['channels' => ['mob', 'facebook', 'whatsapp']];
  }

  /**
   * {@inheritDoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    foreach (static::allChannels() as $id => $info) {
      if (isset($info['example'])) {
        $sample[$id] = $info['example'];
      }

    }
    return $sample;
  }

  /**
   * {@inheritDoc}
   */
  public static function mainPropertyName() {
    return 'mob';
  }

  /**
   * {@inheritDoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {

    $element['channels'] = [
      '#title' => $this->t('Enabled channels'),
      '#type' => 'checkboxes',
      '#options' => $this->channelOptions(),
      '#default_value' => $this->getSetting('channels')
    ];
    $element['channels']['mob']['#disabled'] = TRUE;
    $element['channels']['mob']['#value'] = TRUE;

    return $element;
  }

  /**
   * Make the options for a select or checkbox field.
   */
  public static function channelOptions() {
    foreach (self::allChannels() as $channel => $info) {
      $options[$channel] = $info['title'];
    }
    return $options;
  }

}
