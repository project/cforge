<?php

namespace Drupal\cforge;

use Drupal\taxonomy\Entity\Term;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\field\Entity\FieldConfig;
use Drupal\user\Entity\Role;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Locale\CountryManagerInterface;
use Drupal\Core\Extension\ModuleInstallerInterface;
use Drupal\Core\Extension\ExtensionDiscovery;
use Drupal\Core\Extension\ExtensionList;
use Drupal\Core\Extension\ModuleHandler;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;


ini_set('max_execution_time', 150);

/**
 * Configuration options for each flavour.
 *
 */
class Setup extends FormBase {

  /**
   * The site's default country
   * @var string
   */
  private $country;

  /**
   * @var CountryManagerInterface
   */
  private $countryManager;

  /**
   * @var ModuleInstallerInterface
   */
  private $installer;

  /**
   * @var EntityTypeManagerInterface
   */
  private $entityTypeManager;
  /**
   * @var \Drupal\Core\Extension\ExtensionList
   */
  private $extensionList;

  /**
   * @var
   */
  private $formBuilder;

  /**
   * @var string
   */
  private $defaultLanguage;

  /**
   * @var
   */
  private $moduleHandler;

  /**
   * @var ExtensionDiscovery
   */
  private $extensionDiscovery;

  /**
   * @param ModuleInstallerInterface $module_installer
   * @param EntityTypeManagerInterface $entity_type_manager
   * @param ExtensionList $extension_list
   * @param type $form_builder
   * @param type $language_manager
   * @param ModuleHandler $module_handler
   * @param CountryManagerInterface $country_manager
   */
  public function __construct(ModuleInstallerInterface $module_installer, EntityTypeManagerInterface $entity_type_manager, ExtensionList $extension_list, $form_builder, $language_manager, ModuleHandler $module_handler,  CountryManagerInterface $country_manager) {
    $this->installer = $module_installer;
    $this->entityTypeManager = $entity_type_manager;
    $this->extensionList = $extension_list;
    $this->formBuilder = $form_builder;
    $this->defaultLanguage = $language_manager->getDefaultLanguage()->getId();
    $this->moduleHandler = $module_handler;
    $this->extensionDiscovery = new ExtensionDiscovery(\Drupal::root());
    $this->countryManager = $country_manager;
    $this->country = $this->config('system.date')->get('country.default');

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('module_installer'),
      $container->get('link_generator'),
      $container->get('entity_type.manager'),
      $container->get('extension.list.module'),
      $container->get('form_builder'),
      $container->get('language_manager'),
      $container->get('module_handler'),
      $container->get('country_manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cforge_setup_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // @todo move this to cf_hosted module and put dependencies in composer.json
    $form['migrate'] = [
      '#type' => 'submit',
      '#value' => 'Skip setup and migrate from Drupal 7',
      '#weight' => 0,
      '#submit' => ['::migration']
    ];

    $form['features'] = [
      // Hope that yields the current language.
      '#title' => (string) t('Hamlets features'),
      '#type' => 'checkboxes',
      '#options' => [],
      // Everything enabled by default.
      '#default_value' => [],
      '#weight' => 1,
      '#open' => TRUE,
    ];

    foreach ($this->extensionList->getList() as $module_name => $extension) {
      $info = $extension->info;
      if (@$info['project'] == 'cforge' and $module_name != 'cforge_import' and $module_name != 'cforge_gdpr') {
        if (!$this->moduleHandler->moduleExists($module_name)) {
          $form['features']['#options'][$module_name] = $info['name'];
          $form['features']['#default_value'][] = $module_name;// all Checked by default
          $form['features'][$module_name]['#description'] = $info['description'];
        }
        module_set_weight($module_name, 101);
      }
    }
    module_set_weight('cforge', 100);

    $form['data_content'] = [
      '#title' => $this->t('Prepare the site with'),
      '#type' => 'radios',
      '#options' => [
        'none' => t('No content'),
        'import' => $this->t('CSV Import Tools'),
      ],
      '#default_value'  => 'none',
      '#weight' => 3,
      '#required' => TRUE,
    ];

    $alt_login_form = $this->formBuilder->getForm('Drupal\alt_login\Settings');
    $form['aliases'] = [
      '#title' => $alt_login_form['aliases']['#title'],
      '#type' => 'select',
      '#options' => $alt_login_form['aliases']['#options'],
      '#default_value' => 'address_name',
      '#weight' => 5
    ];

    $form['default_categories'] = array(
      '#title' => $this->t('Preload ad categories'),
      '#description' => $this->t('Can be modified later'),
      '#type' => 'radios',
      '#options' => [
        0 => $this->t('- None -'),
        'en' => 'English LETS',
        'fr' => 'French',
        'be' => 'SEL Belgique, (2 Tier]'
      ],
      '#default_value' => $this->defaultLanguage,
      '#weight' => 6,
      '#states' => [
        'visible' => [
          ':input[name=data_content]' => ['value' => 'none'],
        ],
      ],
    );

    $form['site_default_country'] = [
      '#type' => 'select',
      '#title' => $this->t('Country'),
      '#empty_value' => '',
      '#default_value' => $this->country,
      '#options' => $this->countryManager->getList(),
      '#weight' => 7,
      '#required' => TRUE,
      '#access' => empty($this->country)
    ];

    $form['currency'] = [
      '#type' => 'fieldset',
      '#title' => 'Unit of account',
      'currency_format' => [
        '#title' => $this->t('Main currency type'),
        '#type' => 'radios',
        '#options' => [
          'hours' => $this->t('Hours'),
          'dollars' => $this->t('Dollars and cents'),
          'units' => $this->t('Plain units'),
        ],
        '#default_value' => 'hours',
        '#required' => TRUE,
        '#weight' => 8,
      ],
      'currency_name' => [
        '#title' => $this->t('Currency name'),
        '#description' => $this->t('Use the plural'),
        '#type' => 'textfield',
        '#placeholder' => $this->t('Units'),
        '#weight' => 9
      ]
    ];

    $form['direction'] = [
      '#title' => $this->t('Default transaction direction'),
      '#type' => 'radios',
      '#options' => [
        'credit' => $this->t('Credit (push payment)'),
        'bill' => $this->t('Bill (pull payment)'),
        'both' => $this->t('Both (may be confusing for some users)')
      ],
      '#default_value' => 'credit',
      '#weight' => 10,
    ];

    $form['devel'] = [
      '#title' => t('Devel modules'),
      '#type' => 'checkbox',
      '#weight'=> 15
    ];

    $form['local_admin'] = [
      '#type' => 'details',
      '#title' => t('Create a local admin'),
      '#open' => FALSE,
      '#tree' => TRUE,
      '#weight' => 10,
      'firstname' => [
        '#title' => t('First name'),
        '#type' => 'textfield',
        '#weight' => 1
      ],
      'lastname' => [
        '#title' => t('Last name'),
        '#type' => 'textfield',
        '#weight' => 2
      ],
      'mail' => [
        '#title' => 'email',
        '#type' => 'email',
        '#weight' => 3
      ],
      'message' => [
        '#title' => t('Welcome message'),
        '#type' => 'textarea',
        '#weight' => 4,
        '#default_value' => implode("\n\n", [
          t("Greetings [user:name],"),
          t("We have created your site
      [site:url]"),
          t("And you can login using this temporary address and set your own password
      [user:one-time-login-url]"),
          t("Please appreciate that this software was made, and the hosting service is provided by volunteers who believe that our community infrastructure should not be owned and run as a private business. We offer no legal guarantees but instead we give you full access to all your data, and all the source code so you could host the site yourself or in partnership with like-minded groups.  We promise we will not surrender your personal details voluntarily. Please consider a one-off of or annual donation via http://communityforge.net/donate."),
          '[modules]',
          t("There should be adequate help around the site to get you started, but if you can produce better, do share it with us."),
          t(
             "Data import tools are available, in accordance with the csv formats described here: @url",
             ['@url' => "\n".'http://communityforge.net/'. $this->defaultLanguage .'/prepare-data-for-import'."\n"]
          ),
          t("Do have a go and send the prepared data to me if you have problems."),
          t("When you have obtained a domain name, contact us for further instructions."),
          t("Please sign up to http://helpdesk.communityforge.net [email cc'd] if there are any questions you can answer or need answers to.") ."\n". t("If you no longer want your site, let us know."),
          t("Best Regards, and stay in touch!"),
          t("Community Forge support team")
        ]),
        '#rows' => 10
      ]
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => 'Initiate Hamlets',
      '#weight' => 20,
    ];

    return $form;
  }


  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->moduleHandler->loadInclude('cforge', 'install');
    $available_modules = array_keys($this->extensionDiscovery->scan('module'));

    // Hide the transaction form links
    if ($form_state->getValue('direction') <> 'bill') {
      Role::load('trader')->grantPermission('bill wallets')->save();
    }
    elseif ($form_state->getValue('direction') <> 'credit') {
      Role::load('trader')->grantPermission('credit wallets')->save();
    }

    if ($cat_country = $form_state->getValue('default_categories')) {
      $existing_terms = $this->entityTypeManager
        ->getStorage('taxonomy_term')
        ->loadByProperties(['vid' => SMALLAD_CATEGORIES_VID]);
      // Delete any existing terms in the vocab (one is installed by default)
      foreach ($existing_terms as $term) {
        $term->delete();
      }
      $vocab = Vocabulary::load(SMALLAD_CATEGORIES_VID);
      // Now import the terms into the vocab.
      $get_terms = 'vocab_'.$cat_country;
      $last_term = static::vocabImportRecursive($vocab, $this->$get_terms());
      $vocab->save();
      FieldConfig::load('mc_transaction.mc_transaction.category')
        ->set('default_value', [['target_uuid' => $last_term->uuid()]])
        ->enforceIsNew(FALSE)// Not sure why this is needed
        ->save();
    }
    if ($this->defaultLanguage == 'fr') {
      $this->configFactory()->getEditable('smallads.settings')->set('categories_cardinality', 1)->save();
    }

    $country_code = $form_state->getValue('site_default_country') ?? $this->config('system.date')->get('country.default');
    self::setDefaultCountry($country_code);
    $curr_name = 'Units';
    switch ($form_state->getValue('currency_format')) {
      case 'hours':
        $cformat = ['f1' => '<strong>Hrs</strong>', 'main' => 0, 'f2' => ':', 'subdivision' => '59', 'f3' => ''];
        $curr_name = 'Hours';
        break;
      case 'dollars':
        $cformat = ['f1' => '$', 'main' => 0.99];
        break;
      case 'units':
        $cformat = ['f1' => '<strong>U</strong>', 'main' => 0];
    }
    $this->configFactory()->getEditable('mcapi.settings')
      ->set('currency.name', $form_state->getValue('currency_name') ?: $curr_name)
      ->set('currency.format', $cformat)
      ->save();

    if ($form_state->getvalue('aliases') == 'uid') {
      $alt_login_conf = $this->configfactory()->getEditable('alt_login.settings')
        ->set('display', '[user:address:given_name] [user:address:family_name] ([user:uid])')
        ->save();
    }
    $modules = array_filter($form_state->getValue('features'));
    if ($form_state->getValue('data_content') == 'import') {
      $modules[] = 'cforge_import';
    }

    if ($form_state->getValue('devel')) {
      $modules = array_merge($modules, [
        'dblog',
        'field_ui',
        'views_ui',
        'contextual',
        'admin_toolbar_tools',
        'config'
      ]);
    }
    \Drupal::logger('cforge')->debug('Installing @modules', ['@modules' => print_r($modules, 1)]);
    ini_set('memory_limit', '512M');
    $this->installer->install($modules);

    if (in_array('community_tasks', $modules)) {
      Role::load('trader')->grantPermission('commit to tasks')->save();
    }

    \Drupal::moduleHandler()->loadInclude('cforge', 'install');
    cforge_import_content('cforge', 'new');
    cforge_import_content('cforge', 'all');
    // Set the publiconly on all the page nodes.
    $nids = \Drupal::entityQuery('node')->accessCheck(FALSE)
      ->condition('type', 'page')
      ->condition('nid', 1, '<>') // Exclude privacy policy
      ->execute();
    foreach ($nids as $nid) {
      cforge_node_set_publiconly($nid, TRUE);
    }

    $ladmin = $form_state->getValue('local_admin');
    if ($ladmin['firstname'] and $ladmin['mail']) {
      static::ladmin_create($ladmin['firstname'], $ladmin['lastname'], $ladmin['mail'], $ladmin['message']);
    }

    $form_state->setRedirectUrl(
      Url::fromUserInput($this->config('cforge.settings')->get('member_frontpage'))
    );
  }

  /**
   * Submit callback
   */
  public function migration(array &$form, FormStateInterface $form_state) {
    $modules = [
      'migrate_drupal_ui',
      'migrate_tools',
      'options'
    ];
    $this->installer->install($modules);
    $form_state->setRedirectUrl(Url::fromUserInput('/upgrade'));
  }

  /**
   * Import the terms in a vocabulary or a level of a vocab.
   */
  public static function vocabImportRecursive(Vocabulary $vocab, array $termArray, $parent = 0) : Term {
    $w = $hierarchy = 0;
    foreach ($termArray as $term_name => $term_children) {
      $props = [
        'name' => $term_name,
        'weight' => $w++,
        'vid' => $vocab->id(),
        'parent' => $parent,
        'format' => 'plain_text',
      ];
      $term = Term::Create($props);
      $term->save();
      if (!empty($term_children)) {
        static::vocabImportRecursive($vocab, $term_children, $term->id());
      }
    }
    return $term;
  }

  /**
   * {@inheritdoc}
   */
  public static function vocab_en() {
    return [
      'Arts & Culture' => [],
      'Business Services & Clerical' => [],
      'Clothing' => [],
      'Computing & Electronics' => [],
      'Education & Language' => [],
      'Food' => [],
      'Health & Wellness' => [],
      'House & Garden' => [],
      'LETS administration' => [],
      'Sports & Leisure' => [],
      'Skills & DIY' => [],
      'Transport' => [],
      'Miscellaneous' => []
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function vocab_fr() {
    return [
      'SEL Administration' => [],
      'Alimentation' => [],
      'Artisanat & Bricolage' => [],
      'Arts & Culture' => [],
      'Cours & Langues' => [],
      'Informatique & Eléctro' => [],
      'Maison & Jardin' => [],
      'Mobilité' => [],
      'Santé & Soins' => [],
      'Sports & Evasion' => [],
      'Vêtements plus' => [],
      'Divers' => [],
    ];
  }
  public static function vocab_be() {
    return [
      'A Ménage / Entretien maison' => [
        "Nettoyage/ produits d'entretien" => [],
        'Lessive, repassage' => [],
        'Couture, tricot' => [],
        'Rangement' => [],
        'Ménage - Divers' => [],
      ],
      'B Travaux maisons' => [
        'Travaux lourds' => [],
        'Petits travaux divers' => [],
        'Peinture, tapissage, décoration' => [],
        'Electricité' => [],
        'Plomberie, chauffage' => [],
        'Carrelage, plafonnage, maçonnerie' => [],
        'Menuiserie, planchers, meubles' => [],
        "Isolation, économies d'énergie" => [],
        "Travaux - Divers" => [],
      ],
      'C Enfants / Ados' => [
        'Grossesse et bébé' => [],
        'Baby sitting' => [],
        'Stages, animations créatives' => [],
        'Aide scolaire' => [],
        'Jeux et jouets' => [],
        'Enfants - Divers' => [],
      ],
      'D Alimentation / Gastronomie' => [
        'Nettoyage fruits et légumes' => [],
        'Boissons, soupes, sauces' => [],
        'Plats' => [],
        'Desserts' => [],
        "Tables d'hôtes" => [],
        'Cours, conseils, recettes' => [],
        'Alimentation - Divers' => [],
      ],
      'E Santé / Bien-être / Accompagnement' => [
        'Coiffure, esthétique visage et corps' => [],
        'Massage, thérapies, produits naturels' => [],
        'Personnes âgées, malades, immobilisées' => [],
        "Accompagnement, coaching et conseils 'psy'" => [],
        'Santé - Divers' => [],
      ],
      'F Administration / Gestion' => [
        'Classement de papiers, de dossiers' => [],
        'Dactylographie, mise en page' => [],
        'Courriers, démarches administratives' => [],
        'Assurances, questions juridiques' => [],
        'Fiscalité, pension, chômage' => [],
        'Administration du SEL' => [],
        'Administration - Divers' => [],
      ],
      'G Cours / Formations / Conseils' => [
        'Langues, conversation, traduction' => [],
        'Ecriture, rédaction, orthographe' => [],
        "Recherche d'emploi" => [],
        'Gestion et animation de groupes' => [],
        'Cours - Divers' => [],
      ],
      'H Jardin / Animaux / Nature' => [
        'Jardin : conception, entretien, conseils' => [],
        'Culture potagère, vergers' => [],
        'Récolte de fruits, légumes, plantes' => [],
        "Taille d'arbres et arbustes" => [],
        'Animaux (domestiques -élevage]' => [],
        'Biodiversité, nichoirs, découverte nature' => [],
        'Engrais, semences, plants, produits divers' => [],
        'Jardin et animaux - Divers' => [],
      ],
      'I Arts / Culture / Sports & loisirs' => [
        'Artisanat, peinture, dessin, sculpture' => [],
        'Musique, chant, danse' => [],
        'Photo, vidéo, cinema' => [],
        'Animations, spectacles' => [],
        'Fêtes : conseils, organisation, aide' => [],
        'Promenades, excursions' => [],
        "Jeux (de société, d'extérieur]" => [],
        'Sports' => [],
        'Culture et sport - Divers' => [],
      ],
      'J Transports / Voyages /Hébergement' => [
        'Courses' => [],
        'Covoiturage et transport' => [],
        'Déménagement' => [],
        'Auto-moto-vélo' => [],
        'Voyages, randonnées' => [],
        'Hébergement, échange maisons' => [],
        'Gardiennage maison' => [],
        'Transport - Divers' => [],
      ],
      'K Informatique /Électroménager' => [
        'Dépannage informatique' => [],
        'Réparation électroménager' => [],
        'Formations Internet / email' => [],
        'Formations logiciels' => [],
        'Informatique, électroménager - Divers' => [],
      ],
      'Z Divers (non classées]' => []
    ];
  }

  /**
   * Set the default country on the address field and all existing users to that country.
   */
  static function setDefaultCountry($country_code) {
    $field = FieldConfig::load('user.user.address');//->enforceIsNew(FALSE);
    $settings = $field->get('settings');
    $settings['available_countries'][$country_code] = $country_code;
    $field->set('settings', $settings);
      $field->save();
    $this->configFactory()->getEditable('system.date')->set('country.default', $country_code)->save();
    foreach ($this->entityTypeManager->getStorage('user')->loadMultiple() as $user) {
      if ($user->isAnonymous()) continue;
      $user->address->country_code = $country_code;
      $user->save();
    }
  }

  /**
   * form submission callback
   */
  function ladmin_create($firstname, $lastname, $mail, $message = '') {
    $this->configFactory()->getEditable('system.site')
      ->set('mail', $mail);
    //make the first local admin user.
    $props = [
      'pass' => \Drupal\Core\Password\PasswordGeneratorInterface::generate(),
      'mail' => $mail,
      'init' => $mail,
      'status' => 1,
    ];
    $account = User::create($props);
    $account->setUsername($firstname .' '. $lastname);
    $countries = FieldConfig::load('user.user.address')->getSetting('available_countries');
    $account->address->given_name = $firstname;
    $account->address->family_name = $lastname;
    $account->address->country_code = reset($countries);
    $account->address->dependent_locality = 'Centre';//default neighbourhood
    $account->addRole(RID_TRADER);
    $account->addRole(RID_COMMITTEE);
    $account->addRole(RID_LOCAL_ADMIN);
    $account->save();

    $this->configFactory()->getEditable('cforge.settings')
      ->set('account_manager', $account->id())
      ->save();

    \Drupal::service('plugin.manager.mail')->mail(
      'cf_ladmin',
      'welcome',
      $account->getEmail(),
      $this->defaultLanguage,
      [
        'template' => $message,
        'user' => $account
      ]
    );
  }

}
