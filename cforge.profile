<?php
/**
 * @file
 * Hooks for Cforge distribution profile.
 *
 * @todo ensure all lists show only active users with role RID_TRADER
 */

use Drupal\cforge\MenuParentFormSelector;
use Drupal\mcapi_forms\Entity\McForm;
use Drupal\mcapi\Entity\Transaction;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\aggregator\FeedStorageInterface;
use Drupal\user\UserInterface;
use Drupal\user\Entity\User;
use Drupal\user\Entity\Role;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\Core\Render\Element;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Core\Render\Markup;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\StreamWrapper\StreamWrapperManager;
use Symfony\Component\HttpFoundation\RedirectResponse;

// Roles.
const RID_TRADER = 'trader';
const RID_COMMITTEE = 'committee';
const RID_LOCAL_ADMIN = 'localadmin';

// if the file doesn't exist this will fail silently.
// This could similarly be done using an include in settings.php, but for mass
// hosting easier to do it here
@include '../my_platform.inc';//relative to index.php

/**
 * Implements hook_form_alter().
 */
function cforge_form_alter(&$form, $form_state, $form_id) {
  switch ($form_id) {
    case 'user_login_form':
    case 'user_login_block':
      // Redirect the user to given destination or else the member front page
      $request = \Drupal::request();
      if (!$request->request->has('destination')) {
        $path = \Drupal::config('cforge.settings')->get('member_frontpage');
        $request->query->set('destination', $path);
      }
      break;
    case 'user_form':
    case 'user_profile_form':
    case 'user_register_form':
      $form['account']['mail']['#required'] = TRUE; // why might this not be required?
      break;
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 * Indicate the privacy of the address and coordinates on the user profile edit page.
 */
function cforge_form_user_profile_form_alter(&$form, $form_state) {
  $uid = $form_state->getFormObject()->getEntity()->id();
  if (!\Drupal::service('user.data')->get('cforge', $uid, 'show_address')) {
    $form['address']['widget'][0]['#title'] .= ' ('.t('set to PRIVATE').')';
  }
}

/**
 * Implements hook_user_presave().
 */
function cforge_user_presave(User $account) {
  // If the user is being created, add the trader role.
  if ($account->isNew()) {
    $account->addRole(RID_TRADER);
  }
}

/**
 * Implements hook_form_user_pass_alter().
 *
 * Because $form_state->setRedirect doesn't work while the form is being built.
 */
function cforge_form_user_pass_alter(&$form, $form_state) {
  \Drupal::request()->query->set('destination', '/user/login');
}

/**
 * Implements hook_block_view_BASE_BLOCK_ID_alter().
 *
 * Change the title of the user block.
 */
function cforge_block_view_system_menu_block_alter(&$build, $block) {
  if ($block->getPluginId() == 'system_menu_block:account') {
    $build['#cache']['contexts'][] = 'user';
    $build['#configuration']['label'] = \Drupal::currentUser()->getDisplayName();
  }
}

/**
 * Implements hook_element_info_alter().
 */
function cforge_element_info_alter(&$types) {
  // From the disablepwstrength module
  $processes = &$types['password_confirm']['#process'];
  if (isset($processes)) {
    $position = array_search('user_form_process_password_confirm', $processes);
    if ($position !== FALSE) {
      unset($processes[$position]);
    }
  }
  if (isset($types['processed_text'])) { // otherwise install.php throws an error
    $types['processed_text'] = (array)$types['processed_text'];
    // Some elements need to have their text formats mapped onto cforge text formats
    array_unshift($types['processed_text']['#pre_render'], ['\Drupal\cforge\Secure', 'textFilterMap']);
  }
}

/**
 * Implements hook_entity_info_alter().
 *
 * Add new Form handlers for the user profile edit forms.
 */
function cforge_entity_type_alter(array &$entity_types) {
  if (isset($entity_types['user'])) {
    $entity_types['user']->setFormClass('profile', "Drupal\user\ProfileForm");
    $entity_types['user']->setFormClass('admin', "Drupal\user\ProfileForm");
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function cforge_form_user_form_alter(&$form, $form_state) {
  $form_display = $form_state->get('form_display');
  if ($form_display->getComponent('account')) {
    // @see cforge_entity_base_field_info_alter();
    unset($form['account']['current_pass']);
    $form['account']['status']['#options'][0] = t('User has not been approved or is no longer a member');
    $form['account']['status']['#options'][1] = t('User can log in normally');
    foreach ($form['account']['roles']['#options'] as $rid => &$desc) {
      if ($summary = Role::load($rid)->getThirdpartySetting('cforge', 'summary')) {
        $desc .= ' '. $summary;
      }
    }
  }
  if ($form_display->getComponent('privacy')) {
    $form['privacy']  = [
      '#type' => 'details',
      '#title' => t('Privacy settings'),
      '#description' => t('Determine how much of your data other people can see. Committee members can see everything.'),
      '#open' => TRUE
    ] + cforge_user_privacy_fields($form_state->getFormObject()->getEntity()->id(), $form);
    $form['actions']['submit']['#submit'][] = 'cforge_user_privacy_settings_submit';
  }
  //Can't remember why I did this
  //unset($form['smallad_visibility'],  $form['contact']);

  $user = $form_state->getFormObject()->getEntity();
  // Prevent non-admin from editing account 1 login
  if ($user->id() == 1) {
    if (isset($form['acccount'])) {
      $form['account']['status']['#disabled'] = TRUE;
      if (\Drupal::currentUser()->id() != 1) {
        // Hide everything that only user 1 can change
        $form['account']['name']['#access'] = FALSE;
        $form['account']['pass']['#access'] = FALSE;
        $form['account']['mail']['#access'] = FALSE;
        $form['account']['roles']['#access'] = FALSE;
      }
    }
    // Prevents local admin from editing anything except the user 1 portrait
    // This applies only if we are not user 1 but editing user 1 profile
    if (\Drupal::currentUser()->id() != 1) {
      $ignore = ['portrait', 'actions', 'form_token', 'form_id'];
      foreach (Element::Children($form) as $key) {
        if (in_array($key, $ignore)) {
          continue;
        }
        if (isset($form[$key])) {
          $form[$key]['#access'] = FALSE;
        }
      }
      $form['address']['widget']['#after_build'][] = function($element) {
        \Drupal::logger('temp')->addStatus('Hidden address container0 to prevent non user1 edits to user 1 firstname & lastname');
        $element[0]['address']['container0']['#attributes']['class'][] = 'hidden';
      };
    }
  }
}

/**
 * Utility
 * @param array $form
 */
function cforge_user_privacy_fields($user_id, array $form) : array {
  $subform = [];
  if (isset($form['smallad_visibility'])) {
    $subform['smallad_visibility'] = $form['smallad_visibility'];
  }
  $privacy_settings = \Drupal::service('user.data')->get('cforge', $user_id);
  // The checkboxes show TRUE for visible fields.
  $subform['show_email'] = [
    '#title' => t('Show my main email'),
    '#description' => t('Only other members of the site will be able to see it.'),
    '#type' => 'checkbox',
    '#default_value' => !empty($privacy_settings['show_email'])
  ];
  $subform['show_phones'] = [
    '#title' => t('Show my phone number(s)'),
    '#description' => t('Only other members of the site will be able to see it.'),
    '#type' => 'checkbox',
    '#default_value' => !empty($privacy_settings['show_phones'])
  ];
  // @todo hide only the first line of the address.
  $subform['show_address'] = [
    '#title' => t('Show my address'),
    '#description' => t('Only other members of the site will be able to see it.'),
    '#type' => 'checkbox',
    '#default_value' => !empty($privacy_settings['show_address'])
  ];
  return $subform;
}

/**
 * Implements hook_entity_extra_field_info().
 */
function cforge_entity_extra_field_info() {
  return [
    'user' => [
      'user' => [
        'form' => [
          'privacy' => [
            'label' => t('Privacy settings'),
            'description' => t('Determine how much of your data other people can see. Committee members can see everything.'),
            'weight' => 8
          ]
        ]
      ]
    ]
  ];
}

/**
 * Form submit callback
 */
function cforge_user_privacy_settings_submit($form, $form_state)  {
  $user_data_service = \Drupal::service('user.data');
  $user_entity_id = $form_state->getFormObject()->getEntity()->id();

  foreach (['phones', 'address', 'email'] as $key)  {
    $user_data_service->set('cforge', $user_entity_id, 'show_'.$key, $form_state->getValue('show_'.$key));
  }
  // Assume that the render cache will be cleared as part of the form submission.
}

/**
 * Implements hook field_widget_WIDGET_NAME_form_alter().
 *
 * Tailor the names of the phone multiple cardinality field.
 */
function cforge_field_widget_single_element_telephone_default_form_alter(&$element, $form_state, $context) {
  static $i = 0;
  $element['value']['#title'] = ($i % 2 == 1) ? t('Other phone:') : t('Mobile phone:');
  unset($element['value']['#title_display']);
  $i++;
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Tailor the account settings form.
 */
function cforge_form_user_admin_settings_alter(&$form, $form_state) {
  $reg_fieldset = &$form['registration_cancellation'];
  unset(
    $reg_fieldset['#description'],
    $reg_fieldset['user_register']['#options'][UserInterface::REGISTER_VISITORS]
  );
  $reg_fieldset['#weight'] = -5;
  $reg_fieldset['user_register']['#title'] = t('How to add new members');
  $reg_fieldset['user_register']['#options'][UserInterface::REGISTER_ADMINISTRATORS_ONLY] = t('Only committee can create accounts');
  $reg_fieldset['user_register']['#options'][UserInterface::REGISTER_VISITORS_ADMINISTRATIVE_APPROVAL] = t('Anyone can create an account, then a committee member must enable it');
  $reg_fieldset['user_cancel_method']['#access'] = FALSE;
  $reg_fieldset['user_email_verification']['#access'] = FALSE;
  $form['contact']['#access'] = FALSE;
  $form['admin_role']['#access'] = FALSE;

  $committee = \Drupal::entityTypeManager()->getStorage('user')->loadByProperties(['roles' => 'committee']);
  $options = [];
  foreach ($committee as $user) {
    $options[$user->id()] = $user->getDisplayName();
  }

  if (empty($options)) {
    \Drupal::messenger()->addWarning('No committee members');
  }

}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Other node types have no menus at all.
 */
function cforge_form_node_page_form_alter(&$form, &$form_state) {
  $home = \Drupal::config('system.site')->get('page.front');
  // Prevent changing the alias of the home page, and remove the delete button.
  if ($form['path']['widget'][0]['alias']['#default_value'] == $home) {
    $form['path']['warning'] = [
      '#markup' => t(
        "Warning: '@name' is your site's front page.",
        ['@name' => $home]
      ),
    ];
  }
}

/**
 * Implements hook_form_node_form_alter().
 *
 * Make the descriptions of the aliases more friendly.
 */
function cforge_form_node_form_alter(&$form, $form_state) {
  $node = $form_state->getFormObject()->getEntity();
  if ($node->bundle() == 'page') {// node 1 is privacy policy.
    $form['publiconly'] = [
      '#title' => t('Visible to the public and admins only'),
      '#type' => 'checkbox',
      '#default_value' => cforge_node_get_publiconly($node->id()),
    ];
    foreach (Element::children($form['actions']) as $name) {
      $form['actions'][$name]['#submit'][] = 'cforge_page_submit';
    }
    unset($form['revision']);
  }
  //unset($form['status'], $form['sticky'], $form['options'], $form['author']);
  // Rename the url alias field and explain it better.
  $form['path']['widget'][0]['alias']['#title'] = t('Friendly address');
  $form['path']['widget'][0]['alias']['#description'] = t(
    "Optionally give your page a more meaningful address. E.g. '/summer-holidays'"
  );
  $form['revision_log']['#access'] = FALSE;
}

/**
 * Implements hook_menu_links_discovered_alter().
 */
function cforge_menu_links_discovered_alter(&$links) {
  $entityTypeMan = \Drupal::entityTypeManager();
  unset(
    // Was disabled anyway.
    $links['filter.tips_all'],
    // Was disabled anyway.
    $links['search.view'],
    $links['views_view:views.smallads_directory.page_list'],
    $links['aggregator.page_last'],
    $links['aggregator.feed_add'],
    //$links['admin_toolbar_tools.extra_links:entity.menu.add_form'],
    $links['db_maintenance.page'] // I think this is a bug in the module. this link has no text.
  );
  $links['contact.site_page']['enabled'] = TRUE;

  //Remove unwanted menu items put in the main menu by core modules.
  if (isset($links['user.logout'])) {
    // This overrides admin_toolbar_tools_menu_links_discovered_alter()
    unset($links['user.logout']['parent']);
    $links['user.logout']['menu_name'] = 'account';
  }

  if (isset($links['mcapi.mass'])) {
    $links['mcapi.mass']['weight'] = 1;
  }
  if (isset($links['views_view:views.smallads_directory.collection'])) {
    $links['views_view:views.smallads_directory.collection']['menu_name'] = 'tools';
  }

  if ($entityTypeMan->getDefinition('taxonomy_vocabulary', FALSE)) {
    $link_prefix = $link_id = 'entity.taxonomy_vocabulary.overview_form.';
    if (\Drupal::moduleHandler()->moduleExists('admin_toolbar_tools')) {
      $link_prefix = 'admin_toolbar_tools.extra_links:' . $link_prefix;
    }
    // Create links in the tools menu for these, if the vocabs exist
    // @todo these belong on their respective modules.
    foreach (['galleries', 'binders', 'categories'] as $name) {
      if (isset($links[$link_prefix.$name])) {
        $existing = $links[$link_prefix.$name];
        $vocab = Vocabulary::load($name);
        if (isset($existing)) {
          $links['cf_ladmin.'.$name] = [
            'title' => new TranslatableMarkup('Manage @name', ['@name' => $vocab->label()]),
            'description' => $vocab->getDescription(),
            'menu_name' => 'setup',
            'parent' => ''
          ] + $existing;
        }
      }
      // The admin_toolbar_tools module puts its own links...
      unset($links['entity.taxonomy_term.add_form.' . $name]);
    }
  }
  $links['cforge.logo']['route_parameters']['theme'] = \Drupal::config('system.theme')->get('default');
}

/**
 * Implements hook_local_tasks_alter().
 */
function cforge_local_tasks_alter(&$definitions) {
  unset($definitions['smallads.admin']);//this refers to a view that this distro doesn't use.
  $definitions['entity.user.edit_form']['route_name'] = 'entity.user.profile';

  // Here I already tried to copy the builtin/admin task up,
  // but the builtin task follows the copied task for some reason, so I abandoned it.
  // Remove all the builtin tabs except for admin
  foreach ($definitions as $key => $data) {
    if (isset($data['parent_id']) and $data['parent_id'] == 'mcapi.autotasks') {
      if ($key == 'mcapi_cc.autotasks:form.remote' or  $key == 'mcapi.autotasks:mcapi.form.admin') {
        continue;
      }
      unset($definitions[$key]);
    }
  }
  // Ok but now on mcapi.autotasks:mcapi.form.admin the top level tabs are concealed. [sigh]
}

/**
 * Implements hook_mail().
 */
function cforge_mail($key, &$message, $params) {
  if ($key == 'mass') {
    $message['subject'] = $params['subject'];
    $message['body'][] = \Drupal::token()->replace($params['body'], ['user' => $params['recipient']]);
  }
}

/**
 * Implements hook_mail_alter().
 * Work with new accounts and the account manager.
 */
function cforge_mail_alter(&$message) {
  $cforge_settings = \Drupal::config('cforge.settings');
  if ($acc_manager_uid = $cforge_settings->get('account_manager')) {
    $acc_manager = User::load($acc_manager_uid);
    if ($message['id'] == 'user_register_pending_approval_admin') {
      // Divert it from the site mail to the accounts manager.
      $message['to'] = $acc_manager->getEmail();
      \Drupal::logger('cforge')
        ->debug('Diverted new user message to account manager @mail', ['@mail' => $acc_manager->getEmail()]);
    }
    elseif($message['id'] == 'user_register_pending_approval') {
      // Notify the account manager instead of the user.
      if (\Drupal::Config('user.settings')->get('register') == UserInterface::REGISTER_VISITORS_ADMINISTRATIVE_APPROVAL) {
        $message['to'] = $acc_manager->getEmail();
        \Drupal::logger('cforge')
          ->debug('Diverted new user message to account manager @mail', ['@mail' => $acc_manager->getEmail()]);
      }
      else {
        $message['send'] = FALSE;
        \Drupal::logger('Hamlets')->debug('Diverted new user message to account manager', ['@mail' => $acc_manager->getEmail()]);
      }
    }
  }
  $params = $message['params'];
  $params['user'] = $params['user']??$params['account']??$params['recipient']??User::load(\Drupal::currentUser()->id());
  if ($params['user']->id()) {
    $footer = $cforge_settings->get('mail_footer');
    $message['body'][] = \Drupal::token()->replace($footer, $params);
  }
}

/**
 * Implements hook_form_system_site_information_settings_alter().
 */
function cforge_form_system_site_information_settings_alter(&$form, $form_state) {
  // Add a setting for the member frontpage
  $config = \Drupal::config('cforge.settings');
  $form['front_page']['member_frontpage'] = [
    '#title' => t("Member's front page"),
    '#description' => t("Users will be redirected to this page when they log in."),
    '#default_value' => $config->get('member_frontpage'),
    '#element_validate' => ['cforge_member_frontpage_validate'],
  ] + $form['front_page']['site_frontpage'];
  $form['#submit'][] = 'cforge_submit_system_site_information_settings';
}

/**
 * Element validation callback.
 */
function cforge_member_frontpage_validate(&$element, $form_state) {
  if ($element['#value'] && $element['#value'][0] !== '/') {
    $form_state->setValue('member_frontpage', '/' . $form_state->getValue('member_frontpage'));
  }
}

/**
 * Form submission callback.
 */
function cforge_submit_system_site_information_settings($form, $form_state) {
  \Drupal::configFactory()->getEditable('cforge.settings')
    ->set('member_frontpage', $form_state->getValue('member_frontpage'))
    ->save();
}

/**
 * Implements hook_form_system_modules_alter().
 *
 * Add Readme links to the modules page, where they exist.
 * Put modules into the cforge group.
 */
function cforge_form_system_modules_alter(&$form, &$form_state) {
  // Link to the readme file. (cool idea taken from the advanced help module).
  foreach (Element::children($form['modules']) as $group) {
    foreach (Element::children($form['modules'][$group]) as $module) {
      // This try is temporary while dev systems are in a mess.
      try {
        $path = \Drupal::service('extension.path.resolver')->getPath('module', $module);
      }
      catch (\Exception $e) {
        \Drupal::messenger->warning('Problem with missing module in group: @details', ['@details' => print_r($group, 1)]);
        continue;
      }
      if (file_exists($path . '/README.txt')) {
        $form['modules'][$group][$module]['links']['help'] = [
          '#type' => 'link',
          '#title' => 'README.txt',
          '#url' => Url::fromRoute('cforge.readme', ['module' => $module]),
          '#attributes' => [
            'class' => ['module-link'],
          ],
        ];
      }
      // Move some modules to Hamlets group.
      if (in_array($module, ['community_tasks', 'stringoverrides', 'alt_login', 'asset_injector', 'personal_digest'])) {
        $form['modules']['Hamlets'][$module] = $form['modules'][$group][$module];
        // Can't delete the original without losing the copy.
      }
    }
  }
  if (\Drupal::currentUser()->id() > 1 ) { //i.e. local admin
    foreach (Element::Children($form['modules']) as $group) {
      // Show only the group called 'Community Forge features.
      $form['modules'][$group]['#access'] = $group == 'Hamlets';
    }
  }
}

/**
 * Implements hook_system_modules_uninstall_alter().
 *
 * Local admin can only see hamlets features on module uninstall page.
 */
function cforge_form_system_modules_uninstall_alter(&$form, $form_state) {
  if (\Drupal::currentUser()->id() > 1 ) {
    foreach (array_keys($form['modules']) as $mod) {
      if (substr($mod, 0, 7) != 'cforge_') {
        unset($form['modules'][$mod]);
      }
    }
  }
  \Drupal::messenger()->addWarning('Testing: Please let the support team know if you need to uninstall something not listed here.');
}

/**
 * Utility function to make batch mail a little easier for cforge dependencies.
 *
 * @param string $module
 *   The name of the modue which is responsible for the mail.
 * @param string $key
 *   The key of that mail.
 * @param array $uids
 *   The Uids of the recipients.
 * @param array $params
 *   The mail params.
 * @param string $finished
 *   The path to go to at the end of the batch.
 *
 * @todo test batch mail in d8.
 */
function cforge_batch_mail($module, $key, array $uids, array $params, $finished = '') {
  if (empty($uids)) {
    return;
  }
  $batch_builder = (new BatchBuilder())
    ->setTitle(t('Sending @count mails', ['@count' => count($uids)]))
    ->setProgressMessage(t('Processed @current out of @total.'));

  foreach ($uids as $uid) {
    $batch_builder->addOperation('cforge_batch_mail_send', [$module, $key, $uid, $params]);
  }

  if ($finished) {
    $batch_builder->setFinishCallback($finished);
  }

  batch_set($batch_builder->toArray());
}

/**
 * Batch callback.
 */
function cforge_batch_mail_send($module, $key, $uid, $params, $progress) {
  $account = \Drupal\user\Entity\User::load($uid);
  $params['user'] = $account;
  \Drupal::service('plugin.manager.mail')->mail(
    $module,
    $key,
    $account->getEmail(),
    $account->getPreferredLangcode(),
    $params
  );
}

/**
 * Implements hook_node_access().
 */
function cforge_node_access($node, $op, $account) {
  $result = AccessResult::neutral();
  if ($op == 'view' and $node->bundle() == 'page' and cforge_node_get_publiconly($node->id())) {
    if ($account->hasPermission('administer nodes')) {
      $result = AccessResult::allowed();
    }
    elseif ($account->isAnonymous()) {
      $result = AccessResult::allowed();
    }
    else {// authenticated
      $result = AccessResult::forbidden();
    }
  }
  return $result->cachePerPermissions();
}

/**
 * Implements hook_form_taxonomy_term_categories_form_alter().
 *
 * Force the term description field to be plain text. There doesn't seem to be a
 * way of doing this by altering the baseFieldDefinition.
 */
function cforge_form_taxonomy_term_categories_form_alter(&$form, $form_state) {
  $form['description']['widget'][0]['format']['#allowed_formats'] = ['plain_text'];
}

/**
 * Form submission handler.
 */
function cforge_page_submit($form, $form_state) {
  cforge_node_set_publiconly(
    $form_state->getFormObject()->getEntity()->id(),
    $form_state->getValue('publiconly')
  );
}

/**
 * Implements hook_node_delete().
 */
function cforge_node_delete($node) {
  \Drupal::keyValue('publiconly')->delete($node->id);
}

/**
 * Determine if a node is intended for anon.
 *
 * @param int $nid
 */
function cforge_node_get_publiconly($nid) {
  return \Drupal::keyValue('publiconly')->get($nid, 0);
}

/**
 * Form submit callback.
 *
 * Set the publiconly flag for a node.
 */
function cforge_node_set_publiconly($nid, $value) {
  \Drupal::keyValue('publiconly')
    ->set($nid, $value);
}


/**
 * Implements hook_ENTITY_TYPE_insert().
 *
 * Modify default content view to remove unwanted bulk actions. Unfortunately I
 * can't see how to do this during installation since the view hasn't been created
 * by the time of cforge_install
 */
function cforge_view_insert($view) {
  if ($view->id() == 'content') {
    $display = &$view->getDisplay('default');
    $display['display_options']['fields']['node_bulk_form']['include_exclude'] = 'include';
    $display['display_options']['fields']['node_bulk_form']['selected_actions'] = [
      'node_delete_action',
      'node_publish_action',
      'node_unpublish_action',
    ];
    $view->save();
  }
}

/**
 * Implements hook_migrate_prepare_row().
 */
function cforge_migrate_prepare_row($row, $source, $migration) {
  // This file contains only per_migration hooks, called after this function.
  module_load_include('migrate.inc', 'cforge');
}

 /**
  * Implements hook_migration_plugins_alter().
  */
function cforge_migration_plugins_alter(array &$definitions)  {
  module_load_include('migrate.inc', 'cforge');
  _cforge_migration_plugins_alter($definitions);
}

/**
 * Implements hook_modules_installed().
 * Enable permissions and display components, especially for the non-required modules in the cforge profile.
 */
function cforge_modules_installed($modules) {
  if (in_array('personal_digest', $modules)) {
    // move the personal digest settings from the user profile form to the user display form.
    if ($conf = EntityFormDisplay::load('user.user.default')) {
      // Goes to the bottom.
      $conf->setComponent('personal_digest', ['weight' => 9])->save();
    }
  }
  if (in_array('aggregator', $modules)) {
    Role::load('authenticated')->grantPermission('access news feeds')->save();
    Role::load('anonymous')->grantPermission('access news feeds')->save();
    \Drupal::configFactory()->getEditable('aggregator.settings')
      ->set('items.expire', FeedStorageInterface::CLEAR_NEVER)
      ->save();
  }
  if (in_array('bootstrap_simple_caoursel', $modules)) {
    Role::load('authenticated')->grantPermission('access bootstrap simple carousel')->save();
    Role::load('anonymous')->grantPermission('access bootstrap simple carousel')->save();
  }

  if ($ladmin = Role::load(RID_LOCAL_ADMIN)) {
    if (in_array('asset_injector', $modules)) {
      $ladmin->grantPermission('administer css assets injector')->save();
    }
    if (in_array('murmurations', $modules)) {
      $ladmin->grantPermission('configure murmurations')->save();
    }
    if (\Drupal::moduleHandler()->moduleExists('locale')) {
      $ladmin->grantPermission('translate interface')->save();
    }
  }
}

/**
 * Implements hook_modules_uninstalled().
 */
function cforge_modules_uninstalled($modules) {
  if (in_array('migrate', $modules)) {
    // Delete all the tables beginning with migrate.
    foreach (\Drupal::database()->query("show tables like 'migrate_%'")->fetchCol() as $table) {
      \Drupal::database()->schema()->dropTable($table);
    }
  }
}

/**
 * Implements hook_migrate_destination_info_alter().
 *
 * @temp until something changes in core coz this always fails
 */
function cforge_migrate_destination_info_alter(&$plugins) {
  $plugins['null']['requirements_met'] = TRUE;
}

/**
 * Implements hook_entity_field_access().
 */
function cforge_entity_field_access($operation, $field_definition, $account, $items = NULL) {
  if (!$items or $items->getEntity()->getEntityTypeId() <> 'user') return new Drupal\Core\Access\AccessResultNeutral;
  $fname = $field_definition->getName();
  $result = AccessResult::neutral();
  if ($account->hasPermission('administer users')) {
    $result = AccessResult::Allowed();
  }
  elseif($fname == 'notes_admin') {
    $result = AccessResult::forbidden('Accessible to committee only');
  }
  elseif ($operation == 'view' && !$account->hasPermission('access user profiles')){
    $result = AccessResult::ForbiddenIf(in_array($fname, ['user_picture', 'uid']), 'Anon must never see user info');
  }
  // Check view access for the two private fields
  elseif($operation == 'view' && $fname == 'address') {
    $user_pref = \Drupal::service('user.data')->get('cforge', $account->id(), 'show_address');
    // User 1 skipped this whole hook.
    if ($user_pref or $items->getEntity()->id() == $account->id()) {
      $result = AccessResult::Allowed();
    }
    else {
      $result = AccessResult::Forbidden('Forbidden by user preferences');
    }
  }
  else {
    $result = AccessResult::AllowedIf($operation == 'view');
  }
  return $result->cachePerPermissions();
}

/**
 * Implements hook_block_view_BASE_BLOCK_ID_alter().
 */
function cforge_block_view_aggregator_feed_block_alter(&$build, $block) {
  $build['#pre_render'][] = ['\Drupal\cforge\Secure', 'aggregatorExternalLinks'];
}

/**
 * Element validate callback
 */
function element_validate_cf_is_numeric(&$element, $form_state) {
  if (!is_numeric($element['#value'])) {
    $form_state->setError($element, t('Must be a number.'));
  }
}

/**
 * Implements hook_form_alter().
 * Allow the contact form subject to come from the REQUEST
 */
function cforge_form_contact_message_personal_form_alter(&$form, $form_state) {
  if ($subject = \Drupal::request()->query->get('subject')) {
    $form['subject']['widget'][0]['value']['#default_value'] = $subject;
  }
}

/**
 * Implements hook_comment_presave().
 */
function cforge_comment_presave($comment) {
  $comment->setSubject(trim($comment->getSubject()));
}

/**
 * Implements hook_entity_base_field_info_alter().
 *
 * Clarify the purpose of the username & reduce some long text fields to string
 * fields.
 */
function cforge_entity_base_field_info_alter(&$fields, EntityTypeInterface $entity_type) {
  if ($entity_type->id() === 'user') {
    // Remove the protected fields from the user and hence the need to confirm
    // with a password on the user edit form.
    foreach (['mail', 'pass'] as $fieldname) {
      $constraints = $fields[$fieldname]->getConstraints();
      unset($constraints['ProtectedUserField']);
      $fields[$fieldname]->setConstraints($constraints);
    }
  }
  if ($entity_type == 'taxonomy_term') {
    $base_field_definitions['description'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Description'))
      ->setSettings(array('default_value' => '', 'max_length' => 255));
  }
}

/**
 * Implements hook_ENTITY_TYPE_view_alter().
 */
function cforge_user_view_alter(&$build, $account, $display) {
  if ($account->notes->isEmpty()) {
    unset($build['notes']);
  }
}

/**
 * Implements hook_file_download().
 *
 * Ensure the sql dumps have file headers.
 * Conceal private files from anonymous users.
 */
function cforge_file_download($uri) {
  $target = StreamWrapperManager::getTarget($uri);
  if (substr($target, -4) == '.sql') {
    return [
      'Content-disposition' => 'attachment; filename="' . $target . '"',
    ];
  }
  $scheme = StreamWrapperManager::getScheme($uri);
  if ($scheme == 'private' and \Drupal::currentUser()->isAuthenticated()) {
    if ($file = \Drupal::service('file.repository')->loadByUri($uri)) {
      return file_get_content_headers($file);
    }
  }
  return NULL;
}

/**
 * Implements hook_bootstrap_simple_carousel_items_form_alter().
 */
function cforge_form_bootstrap_simple_carousel_items_form_alter(&$form) {
  $form['actions']['submit']['#submit'][] = 'cforge_clear_block_cache';
}

/**
 * Form submit handler
 */
function cforge_clear_block_cache($form, $form_state) {
  Cache::invalidateTags(['config:block.block.bootstrapsimplecarouselblock']);
}

/**
 * Implements hook_preprocess_views_view_table().
 * Collapse the table columns on narrow screens.
 */
function cforge_preprocess_views_view_table(&$vars) {
  $cols = [];
  foreach ($vars['header'] as $field_name => $info) {
    if ($label = $info['content']) {// The translated label
      $cols["td.views-field-".$vars['fields'][$field_name]] = $label;
    }
  }
  $vars['summary_element'] = cforge_minify_table(
    'div.view-display-id-'.$vars['view']->current_display,
    $cols
  );
  $vars['caption_needed'] = TRUE;
}

/**
 * @param string $css_table_id
 * @param array $cols
 * @return array
 */
function cforge_minify_table($css_table_id, array $cols, $max_width = 300) : array {
  $css = "
  @media screen and (min-width: 1px) and (max-width: {$max_width}px), screen and (min-device-width: 1px) and (max-device-width: {$max_width}px) {
  $css_table_id thead {display:none;}
  $css_table_id table tr td {display: block;}
  $css_table_id td {position: relative; padding-left: 6em;}
  $css_table_id td:before {position: absolute; top: 6px; left: 6px; width: 30%; padding-right: 10px; white-space: nowrap;}\n";
  foreach ($cols as $td_class => $label) {
    $label = str_replace(' ', '\A', $label);
    $css.= "div.$css_table_id $td_class:before { content: \"$label: \"; white-space: pre-wrap;}\n";
  }
  $css.= '}';
  return [
    '#type' => 'inline_template',
    '#template' => '<style>{{ code | raw }}</style>',
    '#context' => [
      'code' => $css,
    ],
  ];
}

/**
 * Implements hook_views_pre_render().
 * Set the title of all views with a user argument
 */
function cforge_views_pre_render(Drupal\views\ViewExecutable $view) {
  if (isset($view->argument['uid'])) {
    $pos = array_search('uid', $view->argument);
    $wid = $view->args[$pos];
    $view->setTitle(User::load($wid)->label());
  }
}

/**
 * Implements hook_user_login().
 */
function cforge_user_login($account) {
  // Redirect according the to the url 'destination'
  $redirect_to = \Drupal::config('cforge.settings')->get('member_frontpage');
  $current_request = Drupal::request();
  // Redirect user 1 to the setup page.
  if ($account->id() == 1) {
    $cats = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadByProperties(['vid' => 'categories']);
    if (empty($cats)) {
      $setup_url =  Url::fromRoute('cforge.setup')->toString();
      \Drupal::messenger()->addWarning(
        t(
          'Hamlets <a href=":url">setup</a> has not yet been completed.',
          [':url' => $setup_url]
        )
      );
      $redirect_to = $setup_url;
    }
  }
  // When not to redirect
  elseif (\Drupal::service('current_route_match')->getRouteName() == 'user.reset.login'
    or Drupal::request()->query->get('destination') <> '/user/login') {
    return;
  }
  $current_request->query->set('destination', $redirect_to);
}


/**
 * Implements hook_block_view_BASE_BLOCK_ID_alter().
 */
function cforge_block_view_bootstrap_simple_carousel_block_alter(array &$build, \Drupal\Core\Block\BlockPluginInterface $block) {
  $build['#cache'] = [
    'tags' => $build['#block']->getCacheTags(),
  ];
}


function cforge_form_bootstrap_simple_carousel_admin_settings_form_alter(&$form, $form_state) {
  $form['#submit'][] = 'cf_clear_carousel_cache';
}
function cf_clear_carousel_cache() {
  \Drupal::service('cache_tags.invalidator')->invalidateTags(['bootstrap_simple_carousel_list', 'block_view']);
}

/**
 * Temporary override of theme_preprocess_views_view_summary_unformatted.
 * When there is more than one argument, the base path of subsequent args doesn't include the previous args.
 * @see https://www.drupal.org/project/drupal/issues/3054944#comment-15540908
 */
function cforge_preprocess_views_view_summary_unformatted(&$variables) {
  $view = $variables['view'];
  $argument = $view->argument[$view->build_info['summary_level']];
  $row_args = [];
  foreach ($variables['rows'] as $id => $row) {
    $row_args[$id] = $argument->summaryArgument($row);
  }
  $argument->processSummaryArguments($row_args);
  foreach ($variables['rows'] as $id => $row) {
    $args = $view->args;
    $args[$argument->position] = $row_args[$id];
    if (!empty($argument->options['summary_options']['base_path'])) {
      $base_path = $argument->options['summary_options']['base_path'];
      $tokens = $view->getDisplay()->getArgumentsTokens();
      $base_path = $argument->globalTokenReplace($base_path, $tokens);
      $url = Url::fromUserInput('/' . $base_path);
      $route = \Drupal::service('router.route_provider')->getRouteByName($url->getRouteName());
      $route_variables = $route->compile()->getVariables(); // arg_0, arg_1
      $parameters = $url->getRouteParameters();
      foreach ($route_variables as $variable_name) {
        if (is_null($parameters[$variable_name])) {
          $parameters[$variable_name] = array_shift($args);
        }
      }
      $variables['rows'][$id]->url = $url->setRouteParameters($parameters)->toString();
    }
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function cforge_form_masquerade_block_form_alter(&$form, $form_state) {
  // Ensures no-one can login above their role.
  $form['autocomplete']['masquerade_as']['#selection_handler'] = 'default:cforge';
}

/**
 * Implements hook_form_FORM_ID_alter().
 * Prevent multilevel menus if using sky_seldulac theme.
 */
function cforge_form_menu_edit_form_alter(&$form, $form_state) {
  if (\Drupal::config('system.theme')->get('default') == 'sky_seldulac') {
    $menu_name = $form_state->getFormObject()->getEntity()->id();
    $flat_menu_names = ['main', 'account', 'tools', 'setup'];
    if (in_array($menu_name, $flat_menu_names)) {
      foreach (Element::Children($form['links']['links']) as $key) {
        $form['links']['links'][$key]['#attributes']['class'][] = 'tabledrag-root';
      }
    }
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * When editing a menu link, force it to the top of the menu tree.
 */
function cforge_form_menu_link_content_menu_link_content_form_alter(&$form, $form_state) {
  if (\Drupal::config('system.theme')->get('default') == 'sky_seldulac') {
    cforge_toplevel_only($form['menu_parent']);
  }
}

/**
 * Utility, remove all the deeper links from the menu_parent widget, leaving only the top level.
 */
function cforge_toplevel_only(array &$element) : void {
  foreach ($element['#options'] as $key => $menu_link_name) {
    if (str_contains($menu_link_name, '--')) {
      unset($element['#options'][$key]);
    }
  }
}

/**
 *
 */
function cforge_theme() {
  return [
    'social_links' => [
      'variables' => [
        'label' => '',
        'links' => []
      ]
    ]
  ];
}

/**
 * Implements hook_preprocess_HOOK() for page.html.twig.
 */
function cforge_preprocess_page(&$vars) {
  if ($social_links = \Drupal::Config('cforge.settings')->get('social_links')) {
    $renderable = [
      '#theme' => 'social_links',
      '#label' => t('Follow us on social media'),
      '#links' => $social_links
    ];
    $vars['socials'] = \Drupal::service('renderer')->render($renderable);
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 * Force the logo and favicon to be globally set.
 */
function cforge_form_system_theme_settings_alter(&$form, $form_state) {
  if ($form['var']['#value'] == 'theme_settings') { // global settings page.
    $form['logo']['default_logo']['#default_value'] = FALSE;
    $form['logo']['default_logo']['#access'] = FALSE;
    $form['favicon']['default_favicon']['#default_value'] = FALSE;
    $form['favicon']['default_favicon']['#access'] = FALSE;
  }
  else {// theme specific settings page.
    unset($form['logo'], $form['favicon']);
    $form['theme_settings']['#access'] = FALSE;
  }
  //unset($form['logo'], $form['favicon']);
}

/**
 * Implements hook_form_FORM_ID_alter().
 * Remove some default configuration options for the contactme field.
 */
function cforge_form_field_config_edit_form_alter(&$form, $form_state) {
  $form['set_default_value']['#access'] = FALSE;
  $form['default_value']['#access'] = FALSE;
  $form['field_storage']['#access'] = FALSE;
  $form['required']['#title'] = t('Require the mobile phone No.');
}

/**
 * Implements hook_preprocess_breakcrumb().
 *
 * Unlink the final breadcrumb.
 */
function cforge_preprocess_breadcrumb(&$vars) {
  if (count($vars['breadcrumb']) > 1) {
    $key = array_key_last($vars['breadcrumb']);
    unset($vars['breadcrumb'][$key]['url']);
  }
}

/**
 * Theme preprocessor.
 * Ensure the branding block uses the globally defined logo, or fallback to cforge logo.
 */
function cforge_preprocess_block(&$vars) {
  // Check if this is the system branding block.
  if ($vars['plugin_id'] === 'system_branding_block') {
    $logo_path = \Drupal::config('system.theme.global')->get('logo.path');
    if (!$logo_path) {
      $logo_path = 'profiles/cforge/assets/logo.png';
    }
    $vars['site_logo'] = \Drupal::service('file_url_generator')->generateString($logo_path);
  }
}
