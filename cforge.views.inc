<?php

/**
 * @file
 * Views hooks.
 */

/**
 * Implements hook_views_data_alter().
 *
 * Populate the neighbourhoods views exposed filter.
 */
function cforge_views_data_alter(&$tables) {
  $tables['user__address']['address_dependent_locality']['filter']['options callback'] = 'cforge_list_neighbourhoods';
  $tables['user__address']['address_dependent_locality']['filter']['id'] = 'in_operator';
}

/**
 * Implements hook_field_views_data_alter();
 *
 * Ensure that address fields expose name columns to views
 *
 * @param array $result
 * @param Drupal\field\Entity\FieldStorageConfig $fieldStorage
 *
 * @temp see https://www.drupal.org/node/2843732
 */
function cforge_field_views_data_alter(array &$result, Drupal\field\Entity\FieldStorageConfig $fieldStorage) {
  if ($fieldStorage->getTypeProvider() == 'address') {
    $columns = [
      'given_name' => 'standard',
      'additional_name' => 'standard',
      'family_name' => 'standard',
    ];
    $field_name = $fieldStorage->getName();
    foreach ($result as $table_name => $table_data) {
      foreach ($columns as $column => $plugin_id) {
        $result[$table_name][$field_name . '_' . $column]['field'] = [
          'id' => $plugin_id,
          'field_name' => $field_name,
          'property' => $column,
        ];
      }
    }
  }
}
