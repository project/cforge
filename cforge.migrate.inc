<?php

use Drupal\migrate\MigrateSkipRowException;
use Drupal\cforge\EventSubscriber\MigrationSubscriber;
use Drupal\user\Entity\User;
use Drupal\migrate\Row;

const CF_SKIP_MIGRATIONS = [
    'action_settings',
    'd7_action',
    'd7_comment_entity_form_display',
    'd7_comment_entity_display',
    'd7_comment_field_instance',
    'd7_filter_settings',
    'd7_menu_link_translation',
    'd7_node:mass_contact',
    'd7_node_complete:mass_contact',
    'd7_node_revision:mass_contact',
    'd7_search_settings',
    'd7_search_page',
    'd7_system_file',
    'd7_system_mail',
    'd7_system_authorize',
    'd7_view_mode',
    'user_profile_field',
    'user_profile_field_instance',
    'user_profile_entity_display',
    'user_profile_entity_form_display'
  ];

 /**
  * Implements hook_migration_plugins_alter().
  */
function _cforge_migration_plugins_alter(&$definitions) {
  $definitions = array_diff_key($definitions, array_flip(CF_SKIP_MIGRATIONS));
  $definitions['d7_block']['process']['region']['map']['sky_seldulac'] = [
    'sky_seldulac' => [
      'help' => 'top',
      'headerright' => null, // this isn't a region any more. see menu "secondary"
      'navigation' => 'top',
      'contentbottom' => 'content',
      'contentfooter' => 'footer',
      'footer' => 'footer'
    ],
    'hbk_cforge' => [
      'help' => 'slider',
      'headerright' => null, // this isn't a region any more. see menu "secondary"
      'navigation' => 'top_header',
      'sidebar_first' => 'sidebar_left',
      'sidebar_second' => 'sidebar_right',
      'contentbottom' => 'content',
      'contentfooter' => 'footer',
      'footer' => 'footer'
    ]
  ];
  $definitions['d7_filter_format']['process']['filters']['id']['map'] = MigrationSubscriber::FILTER_FORMATS;
  $definitions['d7_filter_format']['process']['filters']['id']['default_value'] = 'plain_text';

  $definitions['d7_block']['process']['plugin'][0]['map']['cforge']['cf_training'] = 'hamlets_vid';
  $definitions['d7_block']['process']['plugin'][0]['map']['cforge']['decouvrir'] = 'hamlets_vid';
  $definitions['d7_block']['process']['plugin'][0]['map']['cforge']['nitin'] = 'hamlets_vid';
  $definitions['d7_block']['process']['plugin'][0]['map']['cforge']['sel_promo'] = 'hamlets_vid';
  // This is missing from the poll module
  $definitions['d7_block']['process']['plugin'][0]['map']['poll']['recent'] = 'poll_recent_block';

  // Change the name of the d7 'event_date' field to 'date'
  foreach ($definitions as &$def) {
    if (isset($def['process']['event_date'])) {
      $def['process']['date'] = $def['process']['event_date'];
      $def['process']['date']['process']['value']['to_format'] = 'Y-m-d\TH:i:s';
      unset($def['process']['event_date']);
    }
  }

  $definitions['d7_menu']['process']['id']['map']['secondary-menu'] = 'secondary';
  $definitions['d7_menu']['process']['id']['map']['main'] = 'visitors';
  $definitions['d7_user']['migration_dependencies']['required'][] = 'd7_cf_neighbourhoods';
  // Try to make this last
  $definitions['d7_menu_links']['migration_dependencies']['required'][] = 'd7_cforge_settings';

  unset(
    $definitions['d7_filter_format']['process']['filters']['id']['bypass'],
    // After a week of trying I'm working around this. See MigrationSubsriber::migratePostRowSave
    $definitions['d7_user']['process']['user_picture']
  );
}


/**
 * Implements hook_migrate_prepare_row().
 */
function cforge_migrate_d7_filter_format_prepare_row(Row $row, $source, $migration) {
  $ignore = array_keys(MigrationSubscriber::FILTER_FORMATS);
  if (in_array($row->getSourceProperty('format'), $ignore)) {
    throw new MigrateSkipRowException("Ignoring unknown filter format: ".$row->getSourceProperty('format'));
  }
}

/**
 * Implements hook_migrate_prepare_row().
 */
function cforge_migrate_d7_comment_prepare_row(Row $row, $source, $migration) {
  // Skip comments where the user has been deleted.
  $uid = $row->getSourceProperty('uid');
  if (!User::load($uid)) {
    throw new MigrateSkipRowException("User $uid was not migrated");
  }
}

/**
 * Implements hook_migrate_prepare_row().
 */
function cforge_migrate_d7_menu_prepare_row(Row $row, $source, $migration) {
  if ($row->getSourceProperty('menu_name') == 'visitors') {
    // todo delete visitors menu after menu links have migrated.
    //throw new MigrateSkipRowException("Not migrating visitors menu");
  }
  if ($row->getSourceProperty('menu_name') == 'devel') {
    return FALSE;
  }
}


/**
 * Implements hook_migrate_prepare_row().
 */
function cforge_migrate_d7_menu_links_prepare_row(Row $row, $source, $migration) {
  // Only migrate menu links which are not already in the menu tree.
  // Use the title to identify menu links.
  $dont_mig_paths = ['offers', 'wants', 'members', 'transactions', 'news', 'community-tasks', 'user/logout', 'user'];
  if (in_array($row->getSourceProperty('link_path'), $dont_mig_paths)) {
    throw new MigrateSkipRowException("Link titled '".$row->getSourceProperty('link_path')."': ".$row->getSourceProperty('link_path')." already exists.");
  }
  if (in_array($row->getSourceProperty('menu_name'), ['setup', 'account'])) {
    throw new MigrateSkipRowException("Not migrating links in ".$row->getSourceProperty('menu_name')." menu.");
  }
  if ($row->getSourceProperty('link_path') == 'hamlets_helpdesk') {
    throw new MigrateSkipRowException("hamlets_helpdesk menu link Has been replaced");
  }
  if ($row->getSourceProperty('link_path') ==  'admin/config/system/backup_migrate/export') {
    throw new MigrateSkipRowException("Not migrating backup_migrate link.");
  }
}

/**
 * Implements hook_migrate_prepare_row().
 */
function cforge_migrate_d7_theme_settings_prepare_row(Row $row, $source, $migration) {
  // Copy the logo and favicon settings to global theme settings.
  $settings = $row->getSourceProperty('value');
  if ($settings['logo_path']) {
    \Drupal::configFactory()->getEditable('system.theme.global')->set('logo.path', $settings['logo_path'])->save();
  }
  if ($settings['favicon_path']) {
    \Drupal::configFactory()->getEditable('system.theme.global')->set('favicon.path', $settings['favicon_path'])->save();
  }
  $enabled_themes = array_keys(\Drupal::service('theme_handler')->listInfo());
  // Compilation failed: quantifier does not follow a repeatable item at offset 7
  preg_match('/theme_([a-z_]+)_settings/', $row->getSourceProperty('name'), $matches);
  if (!in_array($matches[1], $enabled_themes)) {
    throw new MigrateSkipRowException("Not migrating settings for not-enabled theme ".$matches[1]);
  }
}

/**
 * Implements hook_migrate_prepare_row().
 */
function cforge_migrate_d7_contact_category_prepare_row(Row $row, $source, $migration) {
  // The category is a machine name taken from the localised d7 category name.
  if (strlen($row->getSourceProperty('category')) > 32) {
    throw new MigrateSkipRowException('Too many contact categories '.$row->id());
  }
}

/**
 * Implements hook_migrate_prepare_row().
 */
function cforge_migrate_d7_block_prepare_row(Row $row, $source, $migration) {
  if ($row->getSourceProperty('theme') <> 'sky_seldulac') {
    throw new MigrateSkipRowException();
  }
  if ($row->getSourceProperty('status') == 0 or $row->getSourceProperty('region') == -1) {
    throw new MigrateSkipRowException('Not migrating disabled blocks: '.$row->getSourceProperty('module') .':'. $row->getSourceProperty('delta'));
  }
  if ($row->getSourceProperty('module') == 'views') {
    throw new MigrateSkipRowException('Not migrating views blocks');
  }
  elseif (in_array($row->getSourceProperty('delta'), ['visitors', 'form', 'online', 'main-menu', 'main'])) {
    throw new MigrateSkipRowException('Skipping block '.$row->getSourceProperty('delta') .' '. $row->getSourceProperty('delta'));
  }
  elseif ($row->getSourceProperty('module') == 'aggregator' and $row->getSourceProperty('delta') < 3) {
    throw new MigrateSkipRowException('Aggregator block not migrated');
  }
  elseif ($row->getSourceProperty('delta') == 'setup') {
    return FALSE;
  }
}

/**
 * Implements hook_migrate_prepare_row().
 * Don't migrate certain roles
 */
function cforge_migrate_d7_user_role_prepare_row(Row $row, $source, $migration) {
  if (in_array($row->getSourceProperty('rid'), [3, 4, 6])) {
    throw new MigrateSkipRowException("Role ".$row->getSourceProperty('rid')." is already installed.");
  }
}

/**
 * Implements hook_migrate_prepare_row().
 */
function cforge_migrate_d7_node_type_prepare_row(Row $row, $source, $migration) {
  $type = $row->getSourceProperty('field_name');
  if (in_array($type, ['page', 'story'])) {
     throw new MigrateSkipRowException("Type $type is already installed.");
  }
}
