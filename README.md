# Setup

There are a few gotchas on top of a normal drupal install for different reasons.

1. This distro _requires_ that a private files directory be configured. Follow instructions in sites/YOURSITE/settings.php and make sure it is writable.
1. The default_content module doesn't work with different languages (AFAICT), so there is a workaround means that the web server has to create a symlink inside profiles/cforge/ where by default, it might not have permission to write. So make sure your web server has permission write there.

   Install as normal via the browser mysite.com/core/install.php

# Architecture.

Hamlets is comprised of discreet, reusable components as far as possible to encourage other developers to build with and around. Also LETS groups are not very ambitious with technology. A LETS is a grass roots community club which exists for its members to exchange things without money. Typically it provides an accounting system, a directory of offers and wants and a newsletter. Its a great social technology for building relationships and strengthening community but does not concern itself with the full spectrum of community activities or with monetary reform. Each one is managed by a usually conservative committee. Most groups have between 50 and 150 members.

This main installation profile aims to provide some basic services and the included modules are optional / replacable features which depend on certain elements of the profile. One theme is included, a heavily modified version of Sky (which doesn't use the standard regions, but should).

The cforge.profile defines many things common to all use cases (LETS). They are realworld membership groups with a management structure and a reluctance to do too much online, A full list follows, and then a brief description of the 'features'.

# In brief
Two node-types are defined, page and story, as in the 'standard' profile.
Two new filters are defined for use with the ckeditor.
5 roles are defined - trader, committee, accountant, local admin, and system. The last one is for non-member accounts because the mutual credit module uses user IDs for accounting.
7 user profile fields.
Some presets are provided for the contact module for ease of setup
Some default pages are provided for ease of setup. Also a block with a video in the appropriate language.
the backup and migrate module allows only downloads and restorations of the whole database dump
Two permissions are provided corresponding to the roles, local admin and committee.
A list of neighbourhoods should be provided to enable localised search

# Builtin features
* Realnames hack - rather than relying on the realnames module, the firstname and secondname (required fields) comprise the username. In community there is no expectation of anonymity. * * The drupal autocomplete function is overridden to support this searching on username, email, or uid.
* Password strength checker is disabled since users found it annoying.
* Login_redirect is implemented in one line of code to take user to a view of recent articles.
* Captcha is configured for account creation
Community Tasks module is included as is.
* Address module has been overridden to include a required 'neighbourhood' field, regardless of country
* User profile fields are spread out accross 3 tabs, My info (default), Settings, and manage, which is only accessible to committee
* Page nodes can be be private or accessible to the public, by repurposing the sticky flag

# Provided theme
The blue and white theme is pretty simple, owing to lack of skilled volunteers.
It works ok on mobile phones.
It contains no logic, so its easy to replace.
Clients have rarely complained.

# Sub-Modules:

* *Currencies module* configures the Community Accounting tool.
it adds a description field, and the offers_wants_categories vocabulary to the transaction entity.
It defines a currency, hours, and two payment forms.
It adds the balance history chart, balances, balance limits and pending transactions to the user profile.
It moves the limits setting to the 'manage' tab on the user profile
Provides a theme callback for balance limts, using google speedometer
moves the masspay menu items to be tabs under path 'transact'
sets the message for pending notifications

* *Broadcast module* allows permitted users to email a node to other users in their neighbourhood, or to all other users.
This is done not using the permission system but from a config field on the cforge settings page.
Users can opt out of these 'broadcast' mail notifications.

* *Docs module* creates a new content type, document, which takes attached files, and a new vocabulary for it.
Documents can only be viewed by authenticated users.
A page view shows documents, by category

* *Events module* creates a content-type, event, and a page view and block view for it.
Node sticky flag is used to indicate whether events should be shown to public

* *Gallery module* creates a content-type, photo, and a vocabulary, galleries for categorising it.
A gallery page leads to individual galleries where each photo can be viewed full size.
A block shows random photos.

* *Import* supports new or migrated groups to import their data and mail all users their login details.
- It enables offers_wants_import, mcapi_import and uif (User import framework) (but doesn't disable them)
- Admin can compose an email, test it
- All users who have never logged in are sent a one-time-login link
- Includes uif hooks for addressfield module

* *offline module* supports users without or with limited internet access in 4 ways
-It allows them to nominate other users who can log in as them, using the masquerade_nominate module.
-it diverts mails to those users to the nominands
-It provides printable views of offers, wants, and members.
-It provides a grid view of addresses, which might be suitable for printing.

Users should check the box in user/%/account/edit to qualify as 'offline'

* *Neighbourhoods* Conceals the address field, phone number and name according to one of three settings.
Admin chooses default access and whether users choose their own.

Sites are also invited to apply to join clearing central to trade with other sites using the Clearing Central module (bundled with the included 'Community Accounting' package). Similarly, email info@clearingcentral.net for more details.

#Altering the display
##READ THIS before editing the css block positions.
CF adaptive theme is based on the business theme, which is based on Drupal's core 'stable' theme. It adapts to the width of the screen, using various breakpoints.
At 769px is the most important breakpoint. The blocks in sidebar2 jump to below the content, and narrower still the blocks in sidebar1 jump above the content. The accountmenu block in sidebar 1 jumps to the very top where it becomes a pullout menu.

The adaptive theme has a left and right sidebar and two footer blocks, side-by-side. The blocks are arranged so that one block appears in footer column, at least on the most important pages. Note that each block is usually placed in a single region, then made visible/invisible in that region for specific pages. As the screen narrows, the left sidebar jumps under the content, then the right sidebar jumps above the content. 

|PAGE|Left foot blocks|Right foot blocks|Right sidebar blocks
|---|---|---|---
anon pages    |cf, global feed|stats  |vid
anon other    |cf, global feed|stats  |vid
|---|---|---|---
transact      |               |       |balance-ometer, pending
user/*        |recent comments|stats  |balance-ometer, pending
offers & wants|recent comments|stats  |balance-ometer, pending
transactions  |recent comments|stats  |balance-ometer, pending
members       |new members    |stats  |comments2me
gallery*      |new members    |stats  |comments2me
news          |events         |stats  |SEL news
events        |new members    |stats  |SEL news
documents     |events         |stats  |SEL news
one photo     |cf, global feed|stats  |SEL news
one document  |cf, global feed|stats  |SEL news
one news      |cf, global feed|stats  |SEL news
one page      |cf, global feed|stats  |SEL news
one event     |cf, global feed|stats  |SEL news
one offer/want|               |stats  |
user/*/edit|||
node/*/edit|||
smallad/*/edit|||

To adjust these settings you must visit each block's settings page. Committee members 2 additional blocks on every page, the 'tools' and the 'masquerade'

Every table column should have a class either priority-high, priority-medium or priority-low, which determines which columns disappear first when the screen is very narrow. For views tables this is determined in each views' table settings.

The carousel which was in the business theme on the front page has been removed and instead we use the bootstrap simple carousel (a block). Add images at /admin/structure/bootstrap_simple_carousel.

# THANKS
This installation profile and some the modules that comprise it are the fruits of 12+ years dedication. Community Forge would like to acknowledge the participation of many individual who have contributed materially or otherwise.

* First of all the users, for tolerating and reporting issues. Users are the best testers, and the only testers in absence of other resources.
* Most of users comprise a several SELs in French speaking Europe, especially SEL du Lac in Geneva and the SELs in Wallonia, Belgium.
* In Belgium, Bernard Simon, who brought many early adopter SELs
* Marie Ullens for testing and protecting me from the Belgians
* Art Brock and Timebanks USA, early adopters of the Community Accounting module
* Route des SEL which brought 3000 user to the Community Accounting module early on
* Tiocan retreat centre outside Geneva.
* Mary Fee, for gathering and understanding requirements over many years
* Danièle Warynski, behind the scenes in Geneva
* Olivier Hetzl, Auroville, for moral and financial support
* Tim Jenkin for building CES and keeping it going for 20+ years.
* Annette Loudon for her consistent advocacy and support
* Stephanie Rearick, for bringing the first time bank to Hamlets
* Many more who hosted me during development - see http://matslats.net/hospitality

